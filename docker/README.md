### How to use

#### TLDR

Pull the image from Dockerhub, start the container and build (within the project root folder):

```
docker pull saminappa/samstorm
docker run -v $(pwd):/build --rm -ti saminappa/samstorm "mkdir build && cd build && cmake .. -GNinja && ninja"
```

#### Build the image yourself

You may also build the image yourself. To build the Docker image, run the following (within the Docker directory):
```docker build .```

To generate the makefiles and compile the software using the Docker image, you may follow the instructions given in [the main README.md file](../README.md), but first you should start the container in interactive mode by running following command (within the project root folder):
```docker run -v $(pwd):/build --rm -ti <docker_image_id> "bash"```

The aforementioned `docker_image_id` can be found from the Docker image build output or from `docker images`
