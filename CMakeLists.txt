cmake_minimum_required(VERSION 3.10)
project(Samstorm LANGUAGES CXX C)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_MODULE_PATH "${CMAKE_MODULE_PATH};${CMAKE_CURRENT_SOURCE_DIR}/cmake")

find_package(PkgConfig REQUIRED)

pkg_search_module(LIBXML2 REQUIRED IMPORTED_TARGET libxml-2.0)
pkg_search_module(FREETYPE2 REQUIRED IMPORTED_TARGET freetype2)

include_directories(.)

add_definitions(-DGLEW_STATIC)

add_subdirectory(samstorm)
