#pragma once

#include <samstorm/misc/common.hpp>

#include <any>

namespace Samstorm
{
namespace IOLoaders
{
std::any loadWav(FileBuffer& fileBuffer);
std::any loadOgg(FileBuffer& fileBuffer);
std::any loadTtfFont(FileBuffer& fileBuffer);
std::any loadPng(FileBuffer& fileBuffer);
std::any loadTmx(FileBuffer& fileBuffer);
std::any loadTsx(FileBuffer& fileBuffer);
void releaseAlBuffer(std::any buffer);
void releaseTtfFont(std::any buffer);
void releasePng(std::any buffer);
void releaseTmx(std::any buffer);
void releaseTsx(std::any buffer);
}
}
