#include <samstorm/io/resources.hpp>
#include <algorithm>

namespace Samstorm
{
namespace Resources
{
namespace
{
std::map<std::string, std::any> resources;
std::map<std::string, std::pair<LoadFn, ReleaseFn>> handlers;
IFileLoader* file;

std::string getExtension(const std::string& str)
{
	std::size_t found = str.rfind(".");

	if(found == std::string::npos)
	{
		LOG(LOG_WARN, "Couldn't deduce file extension for: %s", str.c_str());
		return "";
	}

	// Add +1 so that the dot is not included in the extension
	return str.substr(found + 1);
}
}

void init(IFileLoader* fileLoader)
{
	file = fileLoader;
}

void addResourceHandler(const std::string& fileExtension,
                        LoadFn load,
                        ReleaseFn release)
{
	LOG(LOG_INFO, "creating a handler for file extension %s", fileExtension.c_str());
	LOG_IF(handlers.find(fileExtension) != handlers.end(),
	       LOG_WARN,
	       "Overriding handler for file extension %s", fileExtension.c_str());

	handlers[fileExtension] = std::make_pair(load, release);
}

std::any get(const std::string& name)
{
	std::any data;
	auto iter = resources.find(name);

	if(iter == resources.end())
	{
		LOG(LOG_INFO, "loading resource: %s", name.c_str());

		auto loader = handlers.find(getExtension(name));

		if(loader == handlers.end())
		{
			LOG(LOG_WARN, "Couldn't find loader for extension %s", getExtension(name).c_str());
			return data;
		}

		FileBuffer fileBuffer = file->getFile(name);
		data = loader->second.first(fileBuffer);
		resources[name] = data;
		delete[] fileBuffer.data;
	}
	else
	{
		data = iter->second;
	}

	return data;
}

void release(const std::string& name)
{
	LOG(LOG_INFO, "releasing resource: %s", name.c_str());

	auto release = handlers.find(getExtension(name));
	auto resource = resources.find(name);

	if(release == handlers.end())
	{
		LOG(LOG_INFO,
		    "Could not find handler for extension: %s", getExtension(name).c_str());

		return;
	}

	LOG_IF(resource == resources.end(),
	       LOG_WARN,
	       "Could not find resource: %s, maybe it has already been released?", name.c_str());

	if(release != handlers.end() && resource != resources.end())
	{
		release->second.second(resource->second);
		resources.erase(resource);
	}
}

const std::map<std::string, std::any>& getAll()
{
	return resources;
}
}
}
