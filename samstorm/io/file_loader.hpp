#pragma once

#include <vector>
#include <string>
#include <regex>

#include <samstorm/misc/common.hpp>

namespace Samstorm
{
class IFileLoader
{
public:
	IFileLoader(){}
	virtual ~IFileLoader(){}
	virtual FileBuffer getFile(const std::string& filename) = 0;
	virtual std::vector<FileBuffer> getFilesRegex(const std::regex& pattern) = 0;
};
}
