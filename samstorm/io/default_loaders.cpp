#include <samstorm/io/default_loaders.hpp>
#include <samstorm/io/resource_texture_mapper.hpp>
#include <samstorm/io/resources.hpp>
#include <samstorm/debug/logger.hpp>
#include <AL/alut.h>
#include <vorbis/codec.h>
#include <vorbis/vorbisfile.h>
#include <samstorm/3rdparty/stb/stb_image.h>
extern "C" {
#include <samstorm/3rdparty/tmx/tmx_utils.h>
}
#include <libxml/xmlreader.h>
#include <vector>
#define STB_IMAGE_IMPLEMENTATION
#include <samstorm/3rdparty/stb/stb_image.h>

namespace Samstorm
{
namespace IOLoaders
{
namespace
{
tmx_resource_manager* tmxResManager=nullptr;

size_t oggReadFunc(void* ptr, size_t size, size_t nmemb, void* data)
{
	OggFileBuffer* buffer = (OggFileBuffer*)data;
	size_t bytesToRead = size * nmemb;

	if(bytesToRead > buffer->size - buffer->offset)
		bytesToRead = buffer->size - buffer->offset;

	std::memcpy(ptr, buffer->data + buffer->offset, bytesToRead);
	buffer->offset += bytesToRead;

	return bytesToRead;
}

int oggSeekFunc(void* data, ogg_int64_t offset, int whence)
{
	OggFileBuffer* buffer = (OggFileBuffer*)data;

	if(whence == SEEK_CUR)
		buffer->offset += (size_t)offset;
	else if(whence == SEEK_END)
		buffer->offset = buffer->size - (size_t)offset;
	else if(whence == SEEK_SET)
		buffer->offset = (size_t)offset;
	else
		return -1;

	if(buffer->offset > buffer->size)
	{
		buffer->offset = 0;
		return -1;
	}

	return 0;
}

long oggTellFunc(void* data)
{
	OggFileBuffer* buffer = (OggFileBuffer*)data;
	return buffer->offset;
}
}

std::any loadWav(FileBuffer& fileBuffer)
{
	return ALuint(alutCreateBufferFromFileImage(fileBuffer.data, fileBuffer.size));
}

std::any loadOgg(FileBuffer& fileBuffer)
{
	using namespace std;

	LOG(LOG_INFO,
	    "creating sound buffer for name=%s, size=%i",
	    fileBuffer.name.c_str(), fileBuffer.size);

	constexpr int TEMP_BYTE_ARR_SIZE = 4096;
	ALuint buffer;
	ALenum format;
	ALsizei freq;
	size_t bytesRead = 0;
	OggFileBuffer oggFileBuffer{fileBuffer.data, (size_t)fileBuffer.size, 0};
	ov_callbacks callbacks{oggReadFunc, oggSeekFunc, 0, oggTellFunc};
	OggVorbis_File oggFile;
	vorbis_info* vorbisInfo;
	int bitStream;
	char byteArr[TEMP_BYTE_ARR_SIZE] = {0};
	std::vector<char> byteBuffer;

	alGenBuffers(1, &buffer);
	ov_open_callbacks(&oggFileBuffer, &oggFile, NULL, 0, callbacks);

	vorbisInfo = ov_info(&oggFile, -1);

	freq = vorbisInfo->rate;
	format = vorbisInfo->channels == 1 ? AL_FORMAT_MONO16 :
	                                     AL_FORMAT_STEREO16;

	do
	{
		bytesRead = ov_read(&oggFile,
		                    byteArr,
		                    TEMP_BYTE_ARR_SIZE,
		                    0,
		                    2,
		                    1,
		                    &bitStream);

		byteBuffer.insert(byteBuffer.end(),
		                  byteArr,
		                  byteArr + bytesRead);

	}while(bytesRead > 0);

	LOG(LOG_INFO,
	    "decompressed OGG Vorbis buffer size=%u",
	    byteBuffer.size());

	ov_clear(&oggFile);

	alBufferData(buffer,
	             format,
	             &byteBuffer[0],
	             (ALsizei)byteBuffer.size(),
	             freq);

	return buffer;
}

std::any loadTtfFont(FileBuffer& fileBuffer)
{
	LOG(LOG_INFO, "loading font=%s", fileBuffer.name.c_str());

	FTFont::Font* font = new FTFont::Font(fileBuffer.data, fileBuffer.size);
	FTFont::Buffer pngBuffer;

	font->translateToRGBA();
	pngBuffer = font->getPngBuffer();
	FileBuffer fontBitmapBuffer{fileBuffer.name, pngBuffer.data.get(), pngBuffer.size};

	IO::addResourceTextureMapping(font, std::any_cast<GLuint>(loadPng(fontBitmapBuffer)));
	font->releaseBitmapBuffer();

	return font;
}

std::any loadPng(FileBuffer& fileBuffer)
{
	v2<int> textureSize = {0, 0};
	int channel = 0;
	GLuint texture;

	LOG_IF(fileBuffer.size == 0,
	       LOG_WARN,
	       "trying to load image '%s' of size 0!", fileBuffer.name.c_str());

	unsigned char *image = stbi_load_from_memory(fileBuffer.data,
	                                             fileBuffer.size,
	                                             &textureSize.x,
	                                             &textureSize.y,
	                                             &channel,
	                                             4);

	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);

	LOG(LOG_INFO,
	    "created texture id=%u for filename=%s size=%i",
	    texture, fileBuffer.name.c_str(), fileBuffer.size);

	glTexImage2D(GL_TEXTURE_2D,
	             0,
	             GL_RGBA8,
	             textureSize.x,
	             textureSize.y,
	             0,
	             GL_RGBA,
	             GL_UNSIGNED_BYTE,
	             image);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	if(image)
		stbi_image_free(image);

	glBindTexture(GL_TEXTURE_2D, 0);

	return texture;
}

std::any loadTmx(FileBuffer& fileBuffer)
{
	if(!tmxResManager)
	{
		tmx_alloc_func = realloc;
		tmx_free_func  = free;

		tmxResManager = tmx_make_resource_manager();
	}

	xmlTextReaderPtr reader = xmlReaderForMemory((const char*)fileBuffer.data,
	                                             fileBuffer.size,
	                                             nullptr,
	                                             nullptr,
	                                             0);

	while(xmlTextReaderRead(reader))
	{
		std::string node{(char*)xmlTextReaderConstName(reader)};

		if(node == "tileset")
		{
			std::string source{(char*)xmlTextReaderGetAttribute(reader,
			                                                    (xmlChar*)"source")};

			Resources::get<tmx_tileset*>(source);
		}
	}

	tmx_map* map = tmx_rcmgr_load_buffer(tmxResManager,
	                                     (const char*)fileBuffer.data,
	                                     fileBuffer.size);

	if(!map)
	{
		LOG(LOG_WARN,
		    "tmx_rcmgr_load_buffer failed for %s, tmx_strerr=%s",
		    fileBuffer.name.c_str(), tmx_strerr());

		return nullptr;
	}

	return map;
}

std::any loadTsx(FileBuffer& fileBuffer)
{
	if(!tmxResManager)
	{
		tmx_alloc_func = realloc;
		tmx_free_func  = free;

		tmxResManager = tmx_make_resource_manager();
	}

	tmx_tileset* tileset = nullptr;
	int load = tmx_load_tileset_buffer(tmxResManager,
	                                   (const char*)fileBuffer.data,
	                                   fileBuffer.size,
	                                   fileBuffer.name.c_str());

	LOG_IF(!load,
	       LOG_WARN,
	       "tmx_load_tileset_buffer failed for %s, tmx_strerr=%s",
	       fileBuffer.name.c_str(), tmx_strerr());

	if(load)
	{
		resource_holder* rc = (resource_holder*)hashtable_get(tmxResManager,
		                                                      fileBuffer.name.c_str());
		tileset = (tmx_tileset*)rc->resource.tileset;
		unsigned int texture = Resources::get<unsigned int>(tileset->image->source);
		IO::addResourceTextureMapping(tileset, texture);
	}

	return tileset;
}

void releaseAlBuffer(std::any buffer)
{
	ALuint sound = std::any_cast<ALuint>(buffer);
	alDeleteBuffers(1, &sound);
}

void releaseTtfFont(std::any buffer)
{
	FTFont::Font* data = std::any_cast<FTFont::Font*>(buffer);
	releasePng(IO::getTextureId(data));
	IO::removeResourceTextureMapping(data);
	delete data;
}

void releasePng(std::any buffer)
{
	GLuint texture = std::any_cast<GLuint>(buffer);
	glDeleteTextures(1, &texture);
}

void releaseTmx(std::any buffer)
{
	tmx_map* map = std::any_cast<tmx_map*>(buffer);
	tmx_map_free(map);
}

void releaseTsx(std::any buffer)
{
	tmx_tileset* tileset = std::any_cast<tmx_tileset*>(buffer);
	bool unique = true;
	const std::map<std::string, std::any>& all = Resources::getAll();
	std::map<std::string, std::any>::const_iterator iter = all.begin();

	// @TODO: can we trust that the tileset name is equal to the tileset filename?
	std::string filename = tileset->name;
	filename += ".tsx";

	for(; iter != all.end(); ++iter)
	{
		std::size_t found = iter->first.rfind(".tsx");
		if(found != std::string::npos && found == iter->first.size() - 4)
		{
			tmx_tileset* other = std::any_cast<tmx_tileset*>(iter->second);
			if(other != tileset &&
			   !strcmp(other->image->source, tileset->image->source))
			{
				unique = false;
				break;
			}
		}
	}

	if(unique)
	{
		LOG(LOG_INFO,
		    "found unique linked resource %s, releasing it as well",
		    tileset->image->source);

		Resources::release(tileset->image->source);
	}

	IO::removeResourceTextureMapping(tileset);
	hashtable_rm((xmlHashTablePtr)tmxResManager, filename.c_str(), nullptr);
	free_ts(tileset);
}
}
}
