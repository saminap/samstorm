#include <samstorm/debug/logger.hpp>
#include <samstorm/io/resource_texture_mapper.hpp>

#include <map>

namespace Samstorm
{
namespace IO
{
namespace
{
std::map<void*, unsigned int> resources;
}

unsigned int getTextureId(void* resource)
{
	LOG_IF(resources.find(resource) == resources.end(),
	       LOG_WARN,
	       "Could not find textureID for resource %p", resource);

	return resources.find(resource) != resources.end() ? resources[resource] : 0;
}

void addResourceTextureMapping(void* resource, unsigned int textureId)
{
	LOG(LOG_INFO,
	    "Adding mapping for resource=%p textureID=%u",
	    resource, textureId);

	resources[resource] = textureId;
}

void removeResourceTextureMapping(void* resource)
{
	auto iter = resources.find(resource);

	LOG_IF(iter == resources.end(),
	       LOG_WARN,
	       "Resource texture mapping already deleted for resource=%p",
	       resource);

	if(iter != resources.end())
	{
		resources.erase(iter);
	}
}
}
}
