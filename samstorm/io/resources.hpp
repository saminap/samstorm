#pragma once

#include <samstorm/debug/logger.hpp>
#include <samstorm/misc/common.hpp>
#include <samstorm/io/file_loader.hpp>

#include <any>
#include <typeinfo>

#include <cxxabi.h>

namespace Samstorm
{
namespace Resources
{
typedef std::any (*LoadFn)(FileBuffer&);
typedef void (*ReleaseFn)(std::any);

void init(IFileLoader* fileLoader);
void addResourceHandler(const std::string& extension,
                        LoadFn load,
                        ReleaseFn release=nullptr);
std::any get(const std::string& name);
template<typename T> T get(const std::string& name)
{
	std::any data = get(name);
	T ret;

	try
	{
		ret = std::any_cast<T>(data);
	}
	catch(const std::bad_any_cast& e)
	{
		LOG(LOG_WARN,
		    "%s exception: %s, "
		    "tried to cast to %s, "
		    "correct type %s",
		    name.c_str(),
		    e.what(),
		    abi::__cxa_demangle(typeid(T).name(), 0, 0, nullptr),
		    abi::__cxa_demangle(data.type().name(), 0, 0, nullptr));

		if constexpr(std::is_pointer<T>::value)
			ret = nullptr;
	}

	return ret;
}
const std::map<std::string, std::any>& getAll();
void release(const std::string& name);
}
}
