#include <samstorm/io/tar_file_loader.hpp>
#include <samstorm/3rdparty/simple-tar-lib/tar.hpp>

namespace Samstorm
{
TarFileLoader::TarFileLoader(const std::string& resourceFile)
 : resourceFile(resourceFile)
{
}

FileBuffer TarFileLoader::getFile(const std::string& filename)
{
	Samstar::TarEntry entry = Samstar::getFileFromTar(resourceFile, filename);

	return FileBuffer{entry.filename, entry.data, entry.filesize};
}

std::vector<FileBuffer> TarFileLoader::getFilesRegex(const std::regex& pattern)
{
	std::vector<FileBuffer> files;
	std::vector<Samstar::TarEntry> filesFromTar = Samstar::getFilesFromTarRegex(resourceFile,
	                                                                            pattern);

	for(auto file : filesFromTar)
	{
		files.push_back(FileBuffer{file.filename, file.data, file.filesize});
	}

	return files;
}
}
