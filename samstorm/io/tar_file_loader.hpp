#pragma once

#include <vector>
#include <string>

#include <samstorm/io/file_loader.hpp>

namespace Samstorm
{
class TarFileLoader : public IFileLoader
{
public:
	TarFileLoader(const std::string& resourceFile);
	virtual FileBuffer getFile(const std::string& filename) override;
	virtual std::vector<FileBuffer> getFilesRegex(const std::regex& pattern) override;

private:
	std::string resourceFile;
};
}
