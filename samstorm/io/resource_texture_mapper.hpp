#pragma once

namespace Samstorm
{
namespace IO
{
unsigned int getTextureId(void* resource);
void addResourceTextureMapping(void* resource, unsigned int textureId);
void removeResourceTextureMapping(void* resource);
}
}
