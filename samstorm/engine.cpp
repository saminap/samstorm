#include <samstorm/engine.hpp>
#include <samstorm/events/event_manager.hpp>
#include <samstorm/events/key_event.hpp>
#include <samstorm/events/mouse_button_event.hpp>
#include <samstorm/events/mouse_move_event.hpp>
#include <samstorm/events/mouse_scroll_event.hpp>
#include <samstorm/events/text_input_event.hpp>
#include <samstorm/events/window_resize_event.hpp>
#include <samstorm/sounds/source_pool.hpp>
#include <samstorm/debug/profiling_manager.hpp>
#include <samstorm/debug/profiling.hpp>
#include <samstorm/io/resources.hpp>

namespace Samstorm
{
Engine::Engine(const std::string& windowTitle,
               IFileLoader* fileLoader,
               v2<int> projectionSize)
 : projectionSize(projectionSize)
{
	if(!glfwInit())
		exit(1);

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, REQUIRED_GL_MAJOR_VERSION);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, REQUIRED_GL_MINOR_VERSION);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// @TODO: Does GLFW have something like glfwGetCurrentMonitor()? (in which the engine runs)
	GLFWmonitor* monitor = glfwGetPrimaryMonitor();
	const GLFWvidmode* mode = glfwGetVideoMode(monitor);

	window = glfwCreateWindow(mode->width, mode->height, windowTitle.c_str(), monitor, NULL);
	resizeViewport((int)mode->width, (int)mode->height);

	if(!window)
	{
		glfwTerminate();
		exit(1);
	}

	renderer = new Renderer(window);

	glfwMakeContextCurrent(window);
	glfwSetWindowUserPointer(window, this);
	glfwSetKeyCallback(window, Engine::keyhitCallback);
	glfwSetCharCallback(window, Engine::textInputCallback);
	glfwSetCursorPosCallback(window, Engine::mousePositionCallback);
	glfwSetMouseButtonCallback(window, Engine::mouseButtonCallback);
	glfwSetScrollCallback(window, Engine::mouseScrollCallback);
	glfwSetWindowSizeCallback(window, Engine::windowResizeCallback);
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GLFW_TRUE);

	glfwSwapInterval(1);
	glDisable(GL_DEPTH_TEST);

	if(glewInit() != GLEW_OK)
		exit(1);

	SourcePool::init();

	Resources::init(fileLoader);

	gameStateContainer = GameStateContainer(renderer,
	                                        &shaderManager);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	auto consoleCallbackFunc = std::bind(&Engine::consoleCallback,
	                                     this,
	                                     std::placeholders::_1,
	                                     std::placeholders::_2);

	windowResizeEventListener = EventManager::addListener<WindowResizeEvent>(
	[&](Event* event)
	{
		WindowResizeEvent* e = (WindowResizeEvent*)event;
		const v2<int32_t>& size = e->getSize();
		this->resizeViewport(size.x, size.y);
	});

	Console::registerListener("quit", consoleCallbackFunc);
	Console::registerListener("exit", consoleCallbackFunc);
}

Engine::~Engine()
{
	delete renderer;
	SourcePool::deinit();
	glfwSetWindowShouldClose(window, GLFW_TRUE);
	glfwTerminate();
}

void Engine::run()
{
	Console::init(shaderManager.getShader("color"),
	              shaderManager.getShader("font"),
	              Resources::get<FTFont::Font*>("Pixellari.ttf"));

#ifndef NDEBUG
	Internal::ProfilingManager::init(shaderManager.getShader("font"),
	                                 Resources::get<FTFont::Font*>("Pixellari.ttf"));
#endif

	LOG(LOG_INFO, "about to go to main loop");

	while(isRunning())
	{
		PROFILE_THIS

		Internal::TimerManager::update(glfwGetTime());

		renderer->clear();
		renderer->begin();

		Console::update(timer.delta());

		gameStateContainer.update(timer.delta());
		gameStateContainer.draw();

#ifndef NDEBUG
		Internal::ProfilingManager::render(renderer);
#endif
		Console::render(*renderer);

		renderer->render();
		renderer->swapBuffers();

		EventManager::process();

		SourcePool::update();

		glfwPollEvents();
	}
}

void Engine::resizeViewport(int width, int height)
{
	glViewport(0, 0, width, height);

	GLFWmonitor* monitor = glfwGetPrimaryMonitor();
	const GLFWvidmode* mode = glfwGetVideoMode(monitor);

	v2<float> newProjectionSize = {
		(float)projectionSize.x,
		(float)projectionSize.y
	};

	v2<float> roundedSizeDiff = {
		std::round((float)mode->width/(float)projectionSize.x),
		std::round((float)mode->height/(float)projectionSize.y)
	};

	if(mode->width % (int)projectionSize.x != 0)
		newProjectionSize.x = (float)mode->width/roundedSizeDiff.x;

	if(mode->height % (int)projectionSize.y != 0)
		newProjectionSize.y = (float)mode->height/roundedSizeDiff.y;

	Camera::setProjectionSize(newProjectionSize);
	Camera::setDiffToMonitor(roundedSizeDiff);
}

bool Engine::isRunning() const
{
	return !glfwWindowShouldClose(window) && !gameStateContainer.empty();
}

void Engine::shutdown()
{
	gameStateContainer.clear();
}

void Engine::keyhitCallback(GLFWwindow *window, int key, int scancode, int action, int mods)
{
	EventManager::addEvent(new KeyEvent{key, action});

	if((key == GLFW_KEY_BACKSPACE ||
	    key == GLFW_KEY_ENTER     ||
	    key == GLFW_KEY_TAB       ||
	    key == GLFW_KEY_RIGHT     ||
	    key == GLFW_KEY_LEFT      ||
	    key == GLFW_KEY_UP        ||
	    key == GLFW_KEY_DOWN)     &&
	    action != GLFW_RELEASE)
		Engine::textInputCallback(window, keyToAscii(key));
}

void Engine::textInputCallback(GLFWwindow* window, unsigned int codepoint)
{
	EventManager::addEvent(new TextInputEvent{codepoint});
}

void Engine::mousePositionCallback(GLFWwindow* window, double x, double y)
{
	EventManager::addEvent(new MouseMoveEvent({x, y}));
}

void Engine::mouseButtonCallback(GLFWwindow* window, int button, int action, int mods)
{
	EventManager::addEvent(new MouseButtonEvent(button, action));
}

void Engine::mouseScrollCallback(GLFWwindow* window, double xOffset, double yOffset)
{
	EventManager::addEvent(new MouseScrollEvent(xOffset, yOffset));
}

void Engine::windowResizeCallback(GLFWwindow* window, int width, int height)
{
	EventManager::addEvent(new WindowResizeEvent({width, height}));
}

void Engine::consoleCallback(std::string key, std::vector<std::string>)
{
	if(key == "quit" || key == "exit")
		shutdown();
}

Renderer& Engine::getRenderer()
{
	return *renderer;
}

GameStateContainer& Engine::getGameStateContainer()
{
	return gameStateContainer;
}

ShaderManager& Engine::getShaderManager()
{
	return shaderManager;
}

int Engine::keyToAscii(int key)
{
	int ascii = key;

	switch(key)
	{
	case GLFW_KEY_BACKSPACE:
		ascii = 8;
		break;
	case GLFW_KEY_TAB:
		ascii = 9;
		break;
	case GLFW_KEY_ENTER:
		ascii = 10;
		break;
	}

	return ascii;
}
}
