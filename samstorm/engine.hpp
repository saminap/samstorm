#pragma once

#include <samstorm/graphics/renderer.hpp>
#include <samstorm/graphics/text.hpp>
#include <samstorm/graphics/animation.hpp>
#include <samstorm/graphics/camera.hpp>
#include <samstorm/graphics/shaders/shader_manager.hpp>
#include <samstorm/events/event_listener.hpp>
#include <samstorm/misc/timer.hpp>
#include <samstorm/misc/timer_manager.hpp>
#include <samstorm/game_states/game_state_container.hpp>
#include <samstorm/game_states/game_state.hpp>
#include <samstorm/debug/logger.hpp>
#include <samstorm/debug/console.hpp>
#include <samstorm/io/file_loader.hpp>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <iostream>

namespace Samstorm
{
class Engine
{

public:
	Engine(const std::string& windowTitle,
	       IFileLoader* fileLoader,
	       v2<int> projectionSize={DEFAULT_PROJECTION_WIDTH, DEFAULT_PROJECTION_HEIGHT});
	~Engine();
	void run();
	bool isRunning() const;
	void shutdown();
	Renderer& getRenderer();
	GameStateContainer& getGameStateContainer();
	ShaderManager& getShaderManager();

private:
	Timer timer;
	GLFWwindow *window;
	Renderer* renderer;
	GameStateContainer gameStateContainer;
	ShaderManager shaderManager;
	v2<int> projectionSize;
	std::shared_ptr<EventListener> windowResizeEventListener;

	void resizeViewport(int width, int height);

	static void keyhitCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
	static void textInputCallback(GLFWwindow* window, unsigned int codepoint);
	static void mousePositionCallback(GLFWwindow* window, double x, double y);
	static void mouseButtonCallback(GLFWwindow* window, int button, int action, int mods);
	static void mouseScrollCallback(GLFWwindow* window, double xOffset, double yOffset);
	static void windowResizeCallback(GLFWwindow* window, int width, int height);
	static int keyToAscii(int key);

	void consoleCallback(std::string key, std::vector<std::string>);
};
}
