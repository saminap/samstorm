#include <samstorm/game_states/splash_screen.hpp>
#include <samstorm/game_states/game_state_container.hpp>
#include <samstorm/graphics/camera.hpp>
#include <samstorm/debug/logger.hpp>

namespace Samstorm
{
SplashScreenState::SplashScreenState(GameStateContainer* gameStateContainer,
                                     Renderer*           renderer,
                                     ShaderManager*      shaderManager)
 : IGameState(gameStateContainer, renderer, shaderManager)
{
	light1 = Light({-150.f, -150.f},
	               {300.f, 300.f},
	               {1.0f, 1.0f, 1.0f, 1.0f},
	               shaderManager->getShader("light"),
	               &camera);

	light2 = Light({50.f, 50.f},
	               {300.f, 300.f},
	               {1.0f, 0.0f, 1.0f, 1.0f},
	               shaderManager->getShader("light"),
	               &camera);

	yellowSquare = Drawable(0, 50,
	                        50, 50,
	                        {1, 1, 0, 0.25},
	                        {0,0},
	                        {0,0},
	                        shaderManager->getShader("color"),
	                        0,
	                        &camera,
	                        2);

	light2.setZIndex(5);
	camera.followCoordinates(&light2.getCoordinates());

}

SplashScreenState::~SplashScreenState()
{
}

void SplashScreenState::entered()
{
	LOG(LOG_INFO, "");
}

void SplashScreenState::paused()
{
	LOG(LOG_INFO, "");
}

void SplashScreenState::update(double delta)
{
	camera.update(delta);
}

void SplashScreenState::draw()
{
	renderer->add(&light1);
	renderer->add(&light2);
	renderer->add(&yellowSquare);
}
}
