#include <samstorm/game_states/opengl_wait_state.hpp>

namespace Samstorm
{
OpenGLWaitState::OpenGLWaitState(GameStateContainer* gameStateContainer,
                                 Renderer*           renderer,
                                 ShaderManager*      shaderManager)
 : IGameState(gameStateContainer, renderer, shaderManager)
{
	const GLFWvidmode* mode = glfwGetVideoMode(glfwGetPrimaryMonitor());

	pixel = Drawable(0, 0,
	                 10, mode->height,
	                 {1, 0, 0, 1},
	                 {0,0},
	                 {0,0},
	                 shaderManager->getShader("color"));
}

OpenGLWaitState::~OpenGLWaitState()
{
	LOG(LOG_INFO, "exiting");
}

void OpenGLWaitState::entered()
{
	LOG(LOG_INFO, "entering");
}

void OpenGLWaitState::paused()
{
}

void OpenGLWaitState::update(double delta)
{
	if(!screenReady)
		return;
	else if(framesSinceOpenGLready < numOfFramesToWait)
		++framesSinceOpenGLready;
	else
		IGameState::exit();
}

void OpenGLWaitState::draw()
{
	if(!screenReady)
	{
		renderer->add(&pixel);
		renderer->render();
		screenReady = openGLReady();
		renderer->clear();
		renderer->begin();
		return;
	}
}

void OpenGLWaitState::setFramesToWaitAfterReady(int num)
{
	numOfFramesToWait = num;
}

bool OpenGLWaitState::openGLReady()
{
	GLuint pixelFound;
	glReadBuffer(GL_BACK);
	glReadPixels(0, 0, 1, 1, GL_RED, GL_UNSIGNED_INT, &pixelFound);

	LOG_IF(pixelFound,
	       LOG_INFO,
	       "Screen ready to display graphics, time elapsed %fs", timer.elapsed());

	return pixelFound;
}
}
