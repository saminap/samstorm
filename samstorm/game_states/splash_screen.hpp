#pragma once

#include <samstorm/misc/common.hpp>
#include <samstorm/graphics/drawable.hpp>
#include <samstorm/graphics/text.hpp>
#include <samstorm/graphics/animation.hpp>
#include <samstorm/graphics/light.hpp>
#include <samstorm/graphics/shaders/shader_manager.hpp>
#include <samstorm/game_states/game_state.hpp>

#include <map>
#include <vector>

namespace Samstorm
{
class SplashScreenState : public IGameState
{
public:
	SplashScreenState(GameStateContainer* gameStateContainer,
	                  Renderer*           renderer,
	                  ShaderManager*      shaderManager);
	~SplashScreenState();
	virtual void entered() override;
	virtual void paused() override;
	virtual void update(double delta) override;
	virtual void draw() override;

private:
	Light light1;
	Light light2;
	Drawable yellowSquare;
	Camera camera;


};
}
