#include <samstorm/game_states/game_state.hpp>
#include <samstorm/game_states/game_state_container.hpp>

namespace Samstorm
{
IGameState::IGameState(GameStateContainer* gameStateContainer,
                       Renderer*           renderer,
                       ShaderManager*      shaderManager)
 : gameStateContainer(gameStateContainer),
   renderer(renderer),
   shaderManager(shaderManager)
{

}

bool IGameState::exiting() const
{
	return exited;
}

void IGameState::exit()
{
	exited = true;
}
}
