#pragma once

#include <samstorm/graphics/renderer.hpp>
#include <samstorm/graphics/shaders/shader_manager.hpp>

#include <map>
#include <vector>

namespace Samstorm
{
class GameStateContainer;

class IGameState
{
public:
	IGameState(GameStateContainer* gameStateContainer,
	           Renderer*           renderer,
	           ShaderManager*      shaderManager);
	virtual ~IGameState(){}
	virtual void entered() = 0;
	virtual void paused() = 0;
	virtual void update(double delta) = 0;
	virtual void draw() = 0;
	bool exiting() const;
	void exit();

protected:
	GameStateContainer* gameStateContainer;
	Renderer* renderer;
	ShaderManager* shaderManager;

private:
	bool exited=false;
};
}
