#include <samstorm/game_states/game_state_container.hpp>
#include <samstorm/debug/profiling.hpp>

namespace Samstorm
{
GameStateContainer::GameStateContainer(Renderer*      renderer,
                                       ShaderManager* shaderManager)
 : renderer(renderer),
   shaderManager(shaderManager)
{
}

void GameStateContainer::update(double delta)
{
	PROFILE_THIS

	IGameState* current = top();

	while(current != nullptr && current->exiting())
	{
		erase(current);
		current = top();
	}

	if(empty())
		return;

	if(current != previous)
	{
		auto found = std::find(gameStates.begin(), gameStates.end(), previous);

		// First state pushed or previous state has exited (or pop() called)
		if(found == gameStates.end())
		{
			current->entered();
		}

		// Previous state (or engine user) has pushed a new state into the stack
		else
		{
			previous->paused();
			current->entered();
		}

		previous = current;
	}

	current->update(delta);

	if(current->exiting())
		erase(current);
}

void GameStateContainer::draw()
{
	if(!empty())
		top()->draw();
}

void GameStateContainer::pop()
{
	if(!empty())
		top()->exit();
}

void GameStateContainer::clear()
{
	for(auto state : gameStates)
		state->exit();
}

void GameStateContainer::erase(IGameState* state)
{
	auto found = std::find(gameStates.begin(), gameStates.end(), state);

	if(found != gameStates.end())
	{
		delete state;
		gameStates.erase(found);
	}
}

bool GameStateContainer::empty() const
{
	return gameStates.empty();
}

size_t GameStateContainer::size() const
{
	return gameStates.size();
}

IGameState* GameStateContainer::top()
{
	return !empty() ? gameStates.back() : nullptr;
}

IGameState* GameStateContainer::peek(unsigned int offset)
{
	return offset < size() ? *std::prev(gameStates.end(), offset) : nullptr;
}
}
