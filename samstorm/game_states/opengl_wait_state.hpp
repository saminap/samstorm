#pragma once

#include <samstorm/game_states/game_state.hpp>
#include <samstorm/misc/timer.hpp>

namespace Samstorm
{
class OpenGLWaitState : public IGameState
{
public:
	OpenGLWaitState(GameStateContainer* gameStateContainer,
	                Renderer*           renderer,
	                ShaderManager*      shaderManager);
	~OpenGLWaitState();
	virtual void entered() override;
	virtual void paused() override;
	virtual void update(double delta) override;
	virtual void draw() override;
	void setFramesToWaitAfterReady(int num);

private:
	Drawable pixel;
	bool screenReady=false;
	int numOfFramesToWait=10;
	int framesSinceOpenGLready=0;
	Timer timer;

	bool openGLReady();
};
}
