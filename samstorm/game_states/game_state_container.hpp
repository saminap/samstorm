#pragma once

#include <samstorm/misc/common.hpp>
#include <samstorm/game_states/splash_screen.hpp>
#include <samstorm/game_states/game_state.hpp>
#include <samstorm/graphics/renderer.hpp>
#include <samstorm/graphics/shaders/shader_manager.hpp>

#include <map>
#include <vector>
#include <iterator>

namespace Samstorm
{
class GameStateContainer
{
public:
	GameStateContainer(){}
	GameStateContainer(Renderer*      renderer,
	                   ShaderManager* shaderManager);
	template<typename T> IGameState* push()
	{
		// @TODO: find a way how to include the implementation of
		//        this method to the cpp file without getting linking errors
		T* state = new T(this,
		                 renderer,
		                 shaderManager);

		gameStates.push_back(state);
		return state;
	}
	void pop();
	void clear();
	void update(double delta);
	void draw();
	bool empty() const;
	size_t size() const;
	IGameState* top();
	IGameState* peek(unsigned int offset=1);

private:
	std::vector<IGameState*> gameStates;
	IGameState* previous=nullptr;
	Renderer* renderer;
	ShaderManager* shaderManager;

	void erase(IGameState* state);
};
}
