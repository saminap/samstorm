add_library(samstorm_game_states
	STATIC
		game_state_container.cpp
		game_state.cpp
		opengl_wait_state.cpp
		splash_screen.cpp
)

target_link_libraries(samstorm_game_states
	PUBLIC
		samstorm_debug
		samstorm_graphics
		samstorm_io
		samstorm_misc
)
