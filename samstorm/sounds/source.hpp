#pragma once

#include <samstorm/debug/logger.hpp>

#include <AL/alut.h>

namespace Samstorm
{
class Sound;

class Source
{
public:
	Source();
	~Source();
	void play();
	bool playing();
	void pause();
	void stop();
	void attachSound(Sound* sound);
	void releaseBuffer();
	ALint getPlayOffset() const;
	void setPlayOffset(ALint offset);
	void setLooping(bool looping);
	Sound* getSound();

private:
	ALuint source;
	Sound* sound=nullptr;
};
}
