#include <samstorm/sounds/source.hpp>
#include <samstorm/sounds/sound.hpp>

namespace Samstorm
{
Source::Source()
{
	alGenSources(1, &source);
}

Source::~Source()
{
	alSourceStop(source);
	alSourcei(source, AL_BUFFER, 0);
	alDeleteSources(1, &source);
}

void Source::play()
{
	alSourcePlay(source);
}

bool Source::playing()
{
	ALint state = 0;
	alGetSourcei(source, AL_SOURCE_STATE, &state);

	return state == AL_PLAYING;
}

void Source::pause()
{
	alSourcePause(source);
}

void Source::stop()
{
	alSourceStop(source);
}

void Source::attachSound(Sound* sound)
{
	alSourcei(source, AL_BUFFER, sound->getAlBuffer());
	this->sound = sound;
}

void Source::releaseBuffer()
{
	setLooping(false);
	stop();
	alSourcei(source, AL_BUFFER, 0);
	sound = nullptr;
}

ALint Source::getPlayOffset() const
{
	ALint offset;
	alGetSourcei(source, AL_SAMPLE_OFFSET, &offset);

	return offset;
}

void Source::setPlayOffset(ALint offset)
{
	alSourcei(source, AL_SAMPLE_OFFSET, offset);
}

void Source::setLooping(bool loop)
{
	alSourcei(source, AL_LOOPING, loop);
}

Sound* Source::getSound()
{
	return sound;
}
}
