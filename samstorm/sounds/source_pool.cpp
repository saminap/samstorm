#include <samstorm/sounds/source_pool.hpp>
#include <samstorm/sounds/sound.hpp>

namespace Samstorm
{
namespace SourcePool
{
namespace
{
std::vector<Source*> free;
std::vector<Source*> used;
}

void init(int numOfSources)
{
	alutInit(nullptr, nullptr);

	free.reserve(numOfSources);
	used.reserve(numOfSources);

	for(int i=0; i<numOfSources; ++i)
	{
		free.push_back(new Source());
	}
}

void deinit()
{
	for(auto source : getUsedSources())
	{
		source->getSound()->stop();
	}

	for(auto source : free)
	{
		delete source;
	}

	alutExit();
}

Source* getSource()
{
	if(free.empty())
		return nullptr;

	Source* source = free.back();
	free.pop_back();
	used.push_back(source);

	return source;
}

void freeSource(Source* source)
{
	auto iter = std::find(used.begin(), used.end(), source);

	LOG_IF(iter == used.end(),
	       LOG_WARN,
	       "Trying to free a source that has already been freed");

	if(iter != used.end())
	{
		used.erase(iter);
		free.push_back(*iter);
	}
}

std::vector<Source*> getUsedSources()
{
	return used;
}

void update()
{
	std::vector<Source*> sources = getUsedSources();

	for(auto source : sources)
	{
		if(!source->playing())
			source->getSound()->stop();
	}
}
}
}
