#pragma once

#include <samstorm/sounds/source_pool.hpp>
#include <samstorm/sounds/source.hpp>
#include <samstorm/debug/logger.hpp>

#include <AL/alut.h>
#include <iostream>

namespace Samstorm
{
class Source;

class Sound
{
public:
	Sound(ALuint buffer);
	~Sound();
	void play();
	void pause();
	void stop();
	void resume();
	void loop(bool looping);
	ALuint getAlBuffer() const;
	bool playing() const;
	bool paused() const;

private:
	Source* source=nullptr;
	ALuint buffer;
	ALint playOffset=0;
	bool looping=false;
	bool isPaused=false;
};
}
