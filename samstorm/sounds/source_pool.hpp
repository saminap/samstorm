#pragma once

#include <samstorm/sounds/source.hpp>

#include <AL/alut.h>
#include <iostream>
#include <vector>
#include <algorithm>

namespace Samstorm
{
namespace SourcePool
{
static const int MAX_SOURCES=32;

void init(int numOfSources=MAX_SOURCES);
void deinit();
Source* getSource();
void freeSource(Source* source);
std::vector<Source*> getUsedSources();
void update();
};
}
