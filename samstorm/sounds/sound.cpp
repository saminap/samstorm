#include <samstorm/sounds/sound.hpp>
#include <samstorm/sounds/source.hpp>

namespace Samstorm
{
Sound::Sound(ALuint buffer)
 : buffer(buffer)
{
}

Sound::~Sound()
{
	stop();
}

void Sound::play()
{
	isPaused = false;

	if(source == nullptr)
	{
		source = SourcePool::getSource();

		if(source == nullptr)
		{
			LOG(LOG_WARN, "Could not find free sound source");
			return;
		}
	}
	else
	{
		source->play();
		return;
	}

	source->attachSound(this);
	source->setLooping(looping);
	source->play();
}

void Sound::pause()
{
	if(source == nullptr)
		return;

	source->pause();
	playOffset = source->getPlayOffset();
	source->releaseBuffer();
	SourcePool::freeSource(source);
	source = nullptr;
	isPaused = true;
}

void Sound::stop()
{
	if(source == nullptr)
		return;

	source->stop();
	source->releaseBuffer();
	SourcePool::freeSource(source);
	source = nullptr;
	playOffset = 0;
	isPaused = false;
}

void Sound::resume()
{
	if(!isPaused)
		return;

	play();

	if(source != nullptr)
	{
		source->setPlayOffset(playOffset);
		playOffset = 0;
		isPaused = false;
	}
}

void Sound::loop(bool looping)
{
	this->looping = looping;

	if(source != nullptr)
		source->setLooping(looping);
}

ALuint Sound::getAlBuffer() const
{
	return buffer;
}

bool Sound::playing() const
{
	return source != nullptr ? source->playing() : false;
}

bool Sound::paused() const
{
	return isPaused;
}
}
