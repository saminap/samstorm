#pragma once

#include <samstorm/misc/timer_manager.hpp>

namespace Samstorm
{
class Timer
{
public:
	Timer();
	~Timer();
	double elapsed();
	double delta();

	friend void Internal::TimerManager::update(double);
	friend void Internal::TimerManager::add(Timer*);

protected:
	void update(double value);
	void start(double value);

private:
	double delta_;
	double start_;
	double value;
};
}
