#include <samstorm/misc/timer.hpp>

namespace Samstorm
{
Timer::Timer(): value(0), delta_(0), start_(0)
{
	Internal::TimerManager::add(this);
}

Timer::~Timer()
{
	Internal::TimerManager::remove(this);
}

double Timer::elapsed()
{
	return value-start_;
}

double Timer::delta()
{
	return delta_;
}

void Timer::update(double value)
{
	this->delta_ = value-this->value;
	this->value = value;
}

void Timer::start(double value)
{
	this->start_ = value;
}
}
