#include <samstorm/misc/timer_manager.hpp>
#include <samstorm/misc/timer.hpp>

#include <vector>
#include <algorithm>

namespace Samstorm
{
namespace Internal
{
namespace TimerManager
{
namespace
{
std::vector<::Samstorm::Timer*> timers;
double elapsed=0;
}
void add(::Samstorm::Timer* timer)
{
	timers.push_back(timer);
	timer->start(elapsed);
}

void remove(::Samstorm::Timer* timer)
{
	auto found = std::find(timers.begin(), timers.end(), timer);

	if(found != timers.end())
		timers.erase(found);
}

void update(double delta)
{
	elapsed += delta;

	for(auto timer : timers)
		timer->update(delta);
}
}
}
}
