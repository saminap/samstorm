#pragma once

namespace Samstorm
{
class Timer;
namespace Internal
{
namespace TimerManager
{
void add(::Samstorm::Timer* timer);
void remove(::Samstorm::Timer* timer);
void update(double delta);
}
}
}
