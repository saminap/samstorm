#pragma once

#include <samstorm/3rdparty/glm/mat4x4.hpp>
#include <samstorm/3rdparty/glm/gtc/matrix_transform.hpp>
#include <samstorm/3rdparty/freetype-font/freetype_font.hpp>
#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_STROKER_H

extern "C" {
#include <samstorm/3rdparty/tmx/tmx.h>
}

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <vector>
#include <string>
#include <functional>
#include <map>

namespace Samstorm
{
template <typename T>
struct v2
{
	T x;
	T y;
};

typedef struct
{
	float red;
	float green;
	float blue;
	float alpha;
}Color;

typedef struct
{
	v2<float> coordinates;
	v2<float> textureCoordinates;
	Color color;
}VertexBufferData;

typedef struct
{
	GLint size;
	GLenum type;
	GLsizei stride;
	const GLvoid* pointer;
}VertexAttributeEntry;

typedef struct OpenGLData
{
	GLuint texture;
	GLint textureSlot;
	GLuint vertexBuffer;
	GLuint indexBuffer;
	GLuint vertexArray;
	void* vertexBufferPointer;
}OpenGLData;

namespace EDRAW_LAYER
{
enum
{
	BACKGROUND,
	ON_TOP,

	// Keep this item last
	SIZE
};
}

typedef struct
{
	int key;
	int action;
}KeyInput;

typedef struct
{
	v2<double> coordinates;
	KeyInput keys;
}MouseInput;

typedef struct
{
	std::string name;
	unsigned char* data;
	int size;
}FileBuffer;

typedef struct
{
	unsigned char* data;
	size_t size;
	size_t offset;
}OggFileBuffer;

const unsigned int GL_SPRITE_COUNT = 10000;
const int DEFAULT_PROJECTION_WIDTH = 480;
const int DEFAULT_PROJECTION_HEIGHT = 270;
const int REQUIRED_GL_MAJOR_VERSION = 4;
const int REQUIRED_GL_MINOR_VERSION = 0;
const float MAX_OPENGL_TEXTURE_SIZE = 8192.0f;
const glm::mat4 IDENTITY_MATRIX{1.0f};

template <typename T>
v2<T> operator+(const v2<T>& value, const v2<T>& other)
{
	return v2<T>{
		value.x+other.x,
		value.y+other.y
	};
}

template <typename T>
v2<T> operator-(const v2<T>& value, const v2<T>& other)
{
	return v2<T>{
		value.x-other.x,
		value.y-other.y
	};
}

template <typename T>
v2<T> operator*(const v2<T>& value, const v2<T>& other)
{
	return v2<T>{
		value.x*other.x,
		value.y*other.y
	};
}

template <typename T>
v2<T> operator/(const v2<T>& value, const v2<T>& other)
{
	return v2<T>{
		value.x/other.x,
		value.y/other.y
	};
}

template <typename T>
v2<T> operator+(const v2<T>& value, const T& other)
{
	return v2<T>{
		value.x+other,
		value.y+other
	};
}

template <typename T>
v2<T> operator-(const v2<T>& value, const T& other)
{
	return v2<T>{
		value.x-other,
		value.y-other
	};
}

template <typename T>
v2<T> operator*(const v2<T>& value, const T& other)
{
	return v2<T>{
		value.x*other,
		value.y*other
	};
}

template <typename T>
v2<T> operator/(const v2<T>& value, const T& other)
{
	return v2<T>{
		value.x/other,
		value.y/other
	};
}

inline tmx_tile* getTmxTileByProperty(const tmx_tileset* tileset,
                                      const std::string& property,
                                      const std::string& name)
{
	if(!tileset)
		return nullptr;

	for(tmx_tile* tile = tileset->tiles; tile; ++tile)
	{
		tmx_property* prop = tmx_get_property(tile->properties,
		                                      property.c_str());

		if(prop && prop->type == PT_STRING & !strcmp(prop->value.string, name.c_str()))
		{
			return tile;
		}
	}

	return nullptr;
}

inline tmx_tile* getTmxTileByName(const tmx_tileset* tileset,
                                  const std::string& name)
{
	return getTmxTileByProperty(tileset, "name", name);
}
}
