#include <samstorm/events/key_event.hpp>

#include <GLFW/glfw3.h>

namespace Samstorm
{
KeyEvent::KeyEvent(int32_t key, int32_t action)
 : key(key), action(action)
{
}

int32_t KeyEvent::getKey() const
{
	return key;
}

int32_t KeyEvent::getAction() const
{
	return action;
}

bool KeyEvent::pressed() const
{
	return getAction() == GLFW_PRESS;
}

bool KeyEvent::repeated() const
{
	return getAction() == GLFW_REPEAT;
}

bool KeyEvent::released() const
{
	return getAction() == GLFW_RELEASE;
}
}
