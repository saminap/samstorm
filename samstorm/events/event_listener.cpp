#include <samstorm/events/event_listener.hpp>
#include <samstorm/debug/logger.hpp>

namespace Samstorm
{
EventListener::EventListener(uintptr_t eventId,
                             Event::CallbackFn callback,
                             bool skipConsumedEvents)
 : eventId(eventId),
   callback(callback),
   skipConsumedEvents(skipConsumedEvents)
{
}

EventListener::~EventListener()
{
}

void EventListener::operator()(Event* event)
{
	if(!event->consumed() || !skipConsumedEvents)
		callback(event);
}

uintptr_t EventListener::getEventId() const
{
	return eventId;
}
}
