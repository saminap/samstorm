#include <samstorm/events/mouse_button_event.hpp>

#include <GLFW/glfw3.h>

namespace Samstorm
{
MouseButtonEvent::MouseButtonEvent(int32_t button, int32_t action)
 : button(button), action(action)
{
}

int32_t MouseButtonEvent::getButton() const
{
	return button;
}

bool MouseButtonEvent::pressed() const
{
	return action == GLFW_PRESS;
}

bool MouseButtonEvent::released() const
{
	return action == GLFW_RELEASE;
}
}
