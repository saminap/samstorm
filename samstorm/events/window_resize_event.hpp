#pragma once

#include <samstorm/events/event.hpp>
#include <samstorm/misc/common.hpp>

namespace Samstorm
{
class WindowResizeEvent : public Event
{
public:
	EVENT

	WindowResizeEvent(const v2<int32_t>& size);
	const v2<int32_t>& getSize() const;

private:
	v2<int32_t> size;
};
}
