#pragma once

#include <cstdint>
#include <functional>

namespace Samstorm
{
// @TODO: MinGW does not provide __COUNTER__ macro so we need this rather nasty hack :(
#define EVENT static constexpr uintptr_t id() { return (uintptr_t)__PRETTY_FUNCTION__; } \
              virtual uintptr_t getId() const override { return id(); } \
              virtual const char* getName() const override { return typeid(*this).name(); }

class Event
{
public:
	Event(int32_t ttl=1): ttl(ttl){}
	int32_t& getTtl() { return ttl; }
	void consume() { getTtl() = 0; }
	bool consumed() { return getTtl() <= 0; }

	typedef std::function<void(Event*)> CallbackFn;

	// Use the macro above to implement these in the derived classes
	virtual uintptr_t getId() const = 0;
	virtual const char* getName() const = 0;

private:
	int32_t ttl;
};
}
