#pragma once

#include <samstorm/events/event.hpp>
#include <samstorm/misc/common.hpp>

namespace Samstorm
{
class MouseScrollEvent : public Event
{
public:
	EVENT

	MouseScrollEvent(double xOffset, double yOffset);
	const v2<double>& getOffset() const;

private:
	v2<double> offset;
};
}
