#include <samstorm/events/window_resize_event.hpp>

namespace Samstorm
{
WindowResizeEvent::WindowResizeEvent(const v2<int32_t>& size)
 : size(size)
{
}

const v2<int32_t>& WindowResizeEvent::getSize() const
{
	return size;
}
}
