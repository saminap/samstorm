#include <samstorm/events/mouse_scroll_event.hpp>

namespace Samstorm
{
MouseScrollEvent::MouseScrollEvent(double xOffset, double yOffset)
 : offset{xOffset, yOffset}
{
}


const v2<double>& MouseScrollEvent::getOffset() const
{
	return offset;
}
}
