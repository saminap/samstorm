#include <samstorm/events/text_input_event.hpp>

#include <GLFW/glfw3.h>

namespace Samstorm
{
TextInputEvent::TextInputEvent(uint32_t key)
 : key(key)
{
}

uint32_t TextInputEvent::getKey() const
{
	return key;
}
}
