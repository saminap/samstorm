#pragma once

#include <samstorm/events/event.hpp>
#include <samstorm/misc/common.hpp>

namespace Samstorm
{
class MouseMoveEvent : public Event
{
public:
	EVENT

	MouseMoveEvent(const v2<double>& pos);
	const v2<double>& getPosition() const;

private:
	v2<double> pos;
};
}
