#pragma once

#include <samstorm/events/event.hpp>

#include <functional>

namespace Samstorm
{
class EventListener
{
public:
	EventListener(uintptr_t eventId,
	              Event::CallbackFn callback,
	              bool skipConsumedEvents=true);
	~EventListener();
	void operator()(Event* event);
	uintptr_t getEventId() const;

private:
	uintptr_t eventId;
	Event::CallbackFn callback;
	bool skipConsumedEvents;
};
}
