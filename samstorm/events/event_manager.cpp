#include <samstorm/events/event_manager.hpp>
#include <samstorm/debug/logger.hpp>
#include <samstorm/debug/profiling.hpp>

#include <algorithm>
#include <memory>
#include <vector>
#include <unordered_map>

namespace Samstorm
{
namespace EventManager
{
namespace
{
std::vector<std::unique_ptr<Event>> events;
std::unordered_map<uintptr_t, std::vector<std::shared_ptr<EventListener>>> listeners;

void housekeeping()
{
	auto event = listeners.begin();

	while(event != listeners.end())
	{
		auto listener = event->second.begin();

		while(listener != event->second.end())
		{
			if(listener->unique())
				listener = event->second.erase(listener);
			else
				++listener;
		}

		++event;
	}
}

Event* getPtr(std::unique_ptr<Event>& event)
{
	return event.get();
}
}
void process()
{
	PROFILE_THIS

	std::vector<Event*> copy(events.size());
	std::transform(events.begin(), events.end(), copy.begin(), getPtr);

	for(Event* event : copy)
	{
		auto listener = listeners.find(event->getId());

		if(listener != listeners.end())
		{
			auto callback = listener->second.rbegin();

			while(callback != listener->second.rend())
			{
				if(!callback->unique())
				{
					auto func = callback->get();
					(*func)(event);
				}

				++callback;
			}
		}

		--event->getTtl();
	}

	events.erase(std::remove_if(events.begin(),
	                            events.end(),
	                            [](std::unique_ptr<Event> const& event)
	{
		return event->consumed();
	}), events.end());

	housekeeping();
}

void addEvent(Event* event)
{
	events.push_back(std::unique_ptr<Event>(event));
}

std::shared_ptr<EventListener> addListener(uintptr_t         eventId,
                                           Event::CallbackFn callback,
                                           bool              skipConsumedEvents)
{
	std::shared_ptr<EventListener> handle(new EventListener(eventId,
	                                                        callback,
	                                                        skipConsumedEvents));

	listeners[eventId].push_back(handle);

	LOG(LOG_INFO, "creating listener for event id=%u, ptr=%p", eventId, handle.get());

	return handle;
}

void removeListener(std::shared_ptr<EventListener>& listener)
{
	LOG(LOG_INFO, "deleting %p", listener.get());
	listener.reset();
}
}
}
