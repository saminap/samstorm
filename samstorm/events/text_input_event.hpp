#pragma once

#include <samstorm/events/event.hpp>

namespace Samstorm
{
class TextInputEvent : public Event
{
public:
	EVENT

	TextInputEvent(uint32_t key);
	uint32_t getKey() const;

private:
	uint32_t key;
};
}
