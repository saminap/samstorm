#pragma once

#include <samstorm/events/event.hpp>

namespace Samstorm
{
class KeyEvent : public Event
{
public:
	EVENT

	KeyEvent(int32_t key, int32_t action);
	int32_t getKey() const;
	int32_t getAction() const;
	bool pressed() const;
	bool repeated() const;
	bool released() const;

private:
	int32_t key;
	int32_t action;
};
}
