#pragma once

#include <samstorm/events/event.hpp>
#include <samstorm/events/event_listener.hpp>

#include <memory>

namespace Samstorm
{
namespace EventManager
{
void process();
void addEvent(Event* event);
std::shared_ptr<EventListener> addListener(uintptr_t         eventId,
                                           Event::CallbackFn callback,
                                           bool              skipConsumedEvents=true);
template<typename T>
std::shared_ptr<EventListener> addListener(Event::CallbackFn callback,
                                           bool              skipConsumedEvents=true)
{
	return addListener(T::id(), callback, skipConsumedEvents);
}
void removeListener(std::shared_ptr<EventListener>& listener);
}
}
