#include <samstorm/events/mouse_move_event.hpp>

namespace Samstorm
{
MouseMoveEvent::MouseMoveEvent(const v2<double>& pos)
 : pos(pos)
{
}

const v2<double>& MouseMoveEvent::getPosition() const
{
	return pos;
}
}
