#pragma once

#include <samstorm/events/event.hpp>
#include <samstorm/misc/common.hpp>

namespace Samstorm
{
class MouseButtonEvent : public Event
{
public:
	EVENT

	MouseButtonEvent(int32_t button, int32_t action);
	int32_t getButton() const;
	bool pressed() const;
	bool released() const;

private:
	int32_t button;
	int32_t action;
};
}
