#include <samstorm/engine.hpp>
#include <samstorm/io/default_loaders.hpp>
#include <samstorm/io/resources.hpp>
#include <samstorm/io/tar_file_loader.hpp>
#include <samstorm/game_states/opengl_wait_state.hpp>
#include <samstorm/graphics/shaders/light_shader.hpp>
#include <samstorm/debug/logger.hpp>
#include <sstream>

void initShaders(Samstorm::ShaderManager& shaderManager);
void initVertexAttributes(Samstorm::Renderer& renderer);

int main(int argc, char* argv[])
{
	Samstorm::Logger::logToFile();

	Samstorm::TarFileLoader fileLoader("resources.tar");
	Samstorm::Engine engine("Samstorm", &fileLoader);

	Samstorm::Resources::addResourceHandler("ogg", Samstorm::IOLoaders::loadOgg, Samstorm::IOLoaders::releaseAlBuffer);
	Samstorm::Resources::addResourceHandler("wav", Samstorm::IOLoaders::loadWav, Samstorm::IOLoaders::releaseAlBuffer);
	Samstorm::Resources::addResourceHandler("ttf", Samstorm::IOLoaders::loadTtfFont, Samstorm::IOLoaders::releaseTtfFont);
	Samstorm::Resources::addResourceHandler("png", Samstorm::IOLoaders::loadPng, Samstorm::IOLoaders::releasePng);
	Samstorm::Resources::addResourceHandler("tmx", Samstorm::IOLoaders::loadTmx, Samstorm::IOLoaders::releaseTmx);
	Samstorm::Resources::addResourceHandler("tsx", Samstorm::IOLoaders::loadTsx, Samstorm::IOLoaders::releaseTsx);

	initShaders(engine.getShaderManager());
	initVertexAttributes(engine.getRenderer());

	engine.getGameStateContainer().push<Samstorm::SplashScreenState>();
	engine.getGameStateContainer().push<Samstorm::OpenGLWaitState>();

	engine.run();

	return 0;
}

void initShaders(Samstorm::ShaderManager& shaderManager)
{
	extern char _binary_color_vert_start[];
	extern char _binary_color_frag_start[];
	extern char _binary_color_vert_end[];
	extern char _binary_color_frag_end[];

	extern char _binary_font_vert_start[];
	extern char _binary_font_frag_start[];
	extern char _binary_font_vert_end[];
	extern char _binary_font_frag_end[];

	extern char _binary_texture_vert_start[];
	extern char _binary_texture_frag_start[];
	extern char _binary_texture_vert_end[];
	extern char _binary_texture_frag_end[];

	extern char _binary_light_vert_start[];
	extern char _binary_light_frag_start[];
	extern char _binary_light_vert_end[];
	extern char _binary_light_frag_end[];

	if(!shaderManager.newShader("color",
	                            _binary_color_vert_start,
	                            _binary_color_vert_end,
	                            _binary_color_frag_start,
	                            _binary_color_frag_end))
	{
		std::cout << "failed to load color shader" << std::endl;
	}

	if(!shaderManager.newShader("font",
	                            _binary_font_vert_start,
	                            _binary_font_vert_end,
	                            _binary_font_frag_start,
	                            _binary_font_frag_end))
	{
		std::cout << "failed to load font shader" << std::endl;
	}

	if(!shaderManager.newShader("texture",
	                            _binary_texture_vert_start,
	                            _binary_texture_vert_end,
	                            _binary_texture_frag_start,
	                            _binary_texture_frag_end))
	{
		std::cout << "failed to load texture shader" << std::endl;
	}

	if(!shaderManager.newShader<Samstorm::LightShader>("light",
	                            _binary_light_vert_start,
	                            _binary_light_vert_end,
	                            _binary_light_frag_start,
	                            _binary_light_frag_end))
	{
		std::cout << "failed to load light shader" << std::endl;
	}
}

void initVertexAttributes(Samstorm::Renderer& renderer)
{
	// Init the vertex buffer data
	GLsizei stride = sizeof(Samstorm::VertexBufferData);
	std::vector<Samstorm::VertexAttributeEntry> vertexAttributes{
	    // Coordinates
	    Samstorm::VertexAttributeEntry{2, GL_FLOAT, stride, 0},

	    // Texture coordinates
	    Samstorm::VertexAttributeEntry{
	        2,
	        GL_FLOAT,
	        stride,
	        (void*)offsetof(Samstorm::VertexBufferData, textureCoordinates)
	    },

	    // Color
	    Samstorm::VertexAttributeEntry{
	        4,
	        GL_FLOAT,
	        stride,
	        (void*)offsetof(Samstorm::VertexBufferData, color)
	    }
	};

	renderer.initVertexBuffer(vertexAttributes);
}
