#pragma once

#include <samstorm/misc/common.hpp>
#include <samstorm/graphics/drawable.hpp>

namespace Samstorm
{
class Light : public Drawable
{
public:
	Light();
	Light(const v2<float>& coordinates,
	      const v2<float>& size,
	      const Color& color,
	      Shader* shader,
	      Camera* camera=nullptr);
	void draw(void** vertexBufferPointer) const override;
	void move(v2<float> to) override;
};
}
