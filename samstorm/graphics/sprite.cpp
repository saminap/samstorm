#include <samstorm/graphics/sprite.hpp>
#include <samstorm/io/resource_texture_mapper.hpp>

namespace Samstorm
{
Sprite::Sprite(v2<float>       coordinates,
               v2<float>       size,
               const tmx_tile* tile,
               Shader*         shader,
               Camera*         camera,
               int             zIndex,
               float           rotation)
 : Drawable(coordinates,
            size,
            {0, 0, 0, 0},
            {
              (float)tile->ul_x/(float)tile->tileset->image->width,
              (float)tile->ul_y/(float)tile->tileset->image->height
            },
            {
              (float)tile->tileset->tile_width/(float)tile->tileset->image->width,
              (float)tile->tileset->tile_height/(float)tile->tileset->image->height
            },
            shader,
            IO::getTextureId(tile->tileset),
            camera,
            zIndex,
            rotation)
{
}

}
