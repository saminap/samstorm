#include <samstorm/graphics/line.hpp>
#include <cmath>

namespace Samstorm
{
Line::Line()
{

}

Line::Line(const v2<float>& start,
           const v2<float>& end,
           float width,
           const Color& color,
           Shader* shader,
           Camera* camera,
           int zIndex)
{
	this->textureId = 0;
	this->color = color;
	this->shader = shader;
	this->camera = camera;
	this->zIndex = zIndex;
	this->coordinates = start;
	this->centerOfRotation = {0, 0};
	this->size = {
		std::sqrt(
			std::pow(std::abs(end.x-start.x), 2.0f)+
			std::pow(std::abs(end.y-start.y), 2.0f)),
		width
	};

	double x = std::abs(end.x-start.x);
	double y = std::abs(end.y-start.y);

	this->rotation = atan2(y, x) * 180 / M_PI;

	if(end.x < start.x)
		this->rotation = 180-this->rotation;

	if(end.y < start.y)
		this->rotation *= -1;

	setRotation(this->rotation);
}
}
