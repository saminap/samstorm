#include <samstorm/graphics/drawable.hpp>

namespace Samstorm
{
Drawable::Drawable(const float x,
                   const float y,
                   const float width,
                   const float height,
                   const Color color,
                   v2<float>   textureCoordinates,
                   v2<float>   textureSize,
                   Shader*     shader,
                   GLuint      textureId,
                   Camera*     camera,
                   int         zIndex,
                   float       rotation)
 : coordinates{x, y},
   zIndex(zIndex),
   size{width, height},
   color(color),
   textureCoordinates(textureCoordinates),
   textureSize(textureSize),
   shader(shader),
   textureId(textureId),
   camera(camera),
   centerOfRotation{width/2.0f, height/2.0f}
{
	setRotation(rotation);
}

Drawable::Drawable(v2<float>   coordinates,
                   v2<float>   size,
                   const Color color,
                   v2<float>   textureCoordinates,
                   v2<float>   textureSize,
                   Shader*     shader,
                   GLuint      textureId,
                   Camera*     camera,
                   int         zIndex,
                   float       rotation)
 : Drawable(coordinates.x,
            coordinates.y,
            size.x,
            size.y,
            color,
            textureCoordinates,
            textureSize,
            shader,
            textureId,
            camera,
            zIndex,
            rotation)
{

}

const v2<float>& Drawable::getCoordinates() const
{
	return coordinates;
}

const v2<float>& Drawable::getSize() const
{
	return size;
}

const Color& Drawable::getColor() const
{
	return color;
}

void Drawable::setColor(const Color& color)
{
	this->color = color;
}

const v2<float> Drawable::getTextureAtlasCoordinates() const
{
	return textureCoordinates;
}

const v2<float> Drawable::getTextureSize() const
{
	return textureSize;
}

void Drawable::draw(void** vertexBufferPointer) const
{
	VertexBufferData** position = (VertexBufferData**)vertexBufferPointer;
	VertexBufferData* pointer = (VertexBufferData*)*position;

	v2<float> center = {
		coordinates.x+centerOfRotation.x,
		coordinates.y+centerOfRotation.y
	};
	v2<float> rotatedCoordinates;

	rotatedCoordinates.x =
	    (coordinates.x-center.x)*cosrot-(coordinates.y-center.y)*sinrot+center.x;
	rotatedCoordinates.y =
	    (coordinates.x-center.x)*sinrot+(coordinates.y-center.y)*cosrot+center.y;

	pointer->coordinates = rotatedCoordinates;
	pointer->textureCoordinates = textureCoordinates;
	pointer->color = color;
	++pointer;
	++*position;

	rotatedCoordinates.x =
	    (coordinates.x+size.x-center.x)*cosrot-(coordinates.y-center.y)*sinrot+center.x;
	rotatedCoordinates.y =
	    (coordinates.x+size.x-center.x)*sinrot+(coordinates.y-center.y)*cosrot+center.y;

	pointer->coordinates = rotatedCoordinates;
	pointer->textureCoordinates.x = textureCoordinates.x + textureSize.x;
	pointer->textureCoordinates.y = textureCoordinates.y;
	pointer->color = color;
	++pointer;
	++*position;

	rotatedCoordinates.x =
	    (coordinates.x+size.x-center.x)*cosrot-(coordinates.y+size.y-center.y)*sinrot+center.x;
	rotatedCoordinates.y =
	    (coordinates.x+size.x-center.x)*sinrot+(coordinates.y+size.y-center.y)*cosrot+center.y;

	pointer->coordinates = rotatedCoordinates;
	pointer->textureCoordinates.x = textureCoordinates.x + textureSize.x;
	pointer->textureCoordinates.y = textureCoordinates.y + textureSize.y;
	pointer->color = color;
	++pointer;
	++*position;

	rotatedCoordinates.x =
	    (coordinates.x-center.x)*cosrot-(coordinates.y+size.y-center.y)*sinrot+center.x;
	rotatedCoordinates.y =
	    (coordinates.x-center.x)*sinrot+(coordinates.y+size.y-center.y)*cosrot+center.y;

	pointer->coordinates = rotatedCoordinates;
	pointer->textureCoordinates.x = textureCoordinates.x;
	pointer->textureCoordinates.y = textureCoordinates.y + textureSize.y;
	pointer->color = color;
	++pointer;
	++*position;
}

void Drawable::move(v2<float> to)
{
	coordinates = to;
}

void Drawable::setRotation(float rotation)
{
	this->rotation = rotation;
	sinrot = sin(glm::radians(rotation));
	cosrot = cos(glm::radians(rotation));
}

const float Drawable::getRotation() const
{
	return rotation;
}

void Drawable::setCenterOfRotation(const v2<float>& point)
{
	centerOfRotation = point;
}

const v2<float>& Drawable::getCenterOfRotation() const
{
	return centerOfRotation;
}

void Drawable::setZIndex(int zIndex)
{
	this->zIndex = zIndex;
}

int Drawable::getZIndex() const
{
	return zIndex;
}

GLuint Drawable::getTextureId() const
{
	return textureId;
}

void Drawable::setTextureId(GLuint texture)
{
	this->textureId = texture;
}

void Drawable::setShader(Shader* shader)
{
	this->shader = shader;
}

Shader* Drawable::getShader() const
{
	return shader;
}

Camera* Drawable::getCamera() const
{
	return camera;
}

int Drawable::vertexBufferEntryCount() const
{
	return 1;
}

void Drawable::flipHorizontal()
{
	textureCoordinates.x += textureSize.x;
	textureSize.x = -textureSize.x;
}

void Drawable::flipVertical()
{
	textureCoordinates.y += textureSize.y;
	textureSize.y = -textureSize.y;
}
}
