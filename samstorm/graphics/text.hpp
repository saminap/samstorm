#pragma once

#include <samstorm/graphics/drawable.hpp>
#include <samstorm/graphics/camera.hpp>
#include <samstorm/graphics/shaders/shader.hpp>
#include <samstorm/3rdparty/freetype-font/freetype_font.hpp>

#include <vector>
#include <string>
#include <unordered_map>

namespace Samstorm
{
class Text : public Drawable
{
public:
	Text(const std::string& text,
	     const v2<float>&   coordinates,
	     const Color&       textColor,
	     const Color&       borderColor,
	     Shader*            shader,
	     FTFont::Font*      font,
	     Camera*            camera=nullptr,
	     unsigned int       borderSize=0,
	     float              scale=1.0f,
	     int                zIndex=0);

	void draw(void** vertexBufferPointer) const override;
	void move(v2<float> to) override;
	void setColor(const Color& color) override;
	void updateText(const std::string& newText);
	void append(const std::string& appendedText);
	void pop();
	const std::string& getText() const;
	const Color& getBorderColor() const;
	unsigned int getBorderSize() const;
	const FTFont::Font* getFont() const;
	int vertexBufferEntryCount() const override;

private:
	std::string text;
	std::vector<Drawable> characters;
	std::vector<Drawable> borders;
	Color borderColor;
	FTFont::Font* font=nullptr;
	unsigned int borderSize;
	float scale;
	v2<float> initialCoordinates;
	std::string fontName;

	void calculateSize();
};
}
