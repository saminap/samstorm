#include <samstorm/graphics/light.hpp>

namespace Samstorm
{
Light::Light()
{
}

Light::Light(const v2<float>& coordinates,
             const v2<float>& size,
             const Color& color,
             Shader* shader,
             Camera* camera)
{
	this->size = size;
	this->color = color;
	this->shader = shader;
	this->zIndex = 0;
	this->textureId = 0;
	this->camera = camera;

	move(coordinates);
}

void Light::draw(void** vertexBufferPointer) const
{
	VertexBufferData** position = (VertexBufferData**)vertexBufferPointer;
	VertexBufferData* pointer = (VertexBufferData*)*position;

	pointer->coordinates = coordinates;
	pointer->textureCoordinates = {-1.0f, -1.0f};
	pointer->color = color;
	++pointer;
	++*position;

	pointer->coordinates.x = coordinates.x + size.x;
	pointer->coordinates.y = coordinates.y;
	pointer->textureCoordinates = {1.0f, -1.0f};
	pointer->color = color;
	++pointer;
	++*position;

	pointer->coordinates.x = coordinates.x + size.x;
	pointer->coordinates.y = coordinates.y + size.y;
	pointer->textureCoordinates = {1.0f, 1.0f};
	pointer->color = color;
	++pointer;
	++*position;

	pointer->coordinates.x = coordinates.x;
	pointer->coordinates.y = coordinates.y + size.y;
	pointer->textureCoordinates = {-1.0f, 1.0f};
	pointer->color = color;
	++pointer;
	++*position;
}

void Light::move(v2<float> to)
{
	this->coordinates.x = to.x-size.x/2.0f;
	this->coordinates.y = to.y-size.y/2.0f;
}
}
