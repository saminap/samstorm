add_subdirectory(glsl)

add_library(samstorm_graphics_shaders
	STATIC
		shader.cpp
		light_shader.cpp
		shader_manager.cpp
)

target_link_libraries(samstorm_graphics_shaders
	PUBLIC
		samstorm_3rdparty
		samstorm_misc
		samstorm_graphics_shaders_glsl
)
