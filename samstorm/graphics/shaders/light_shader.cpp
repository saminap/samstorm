#include <samstorm/graphics/shaders/light_shader.hpp>

namespace Samstorm
{
void LightShader::preRender()
{
	glGetIntegerv(GL_BLEND_DST_ALPHA, &prevBlendFunc);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE);
}

void LightShader::postRender()
{
	glBlendFunc(GL_SRC_ALPHA, prevBlendFunc);
}
}
