#include <samstorm/graphics/shaders/shader_manager.hpp>

namespace Samstorm
{
ShaderManager::ShaderManager()
{
}

ShaderManager::~ShaderManager()
{
	auto copy = shaders;
	for(auto& entry : copy)
	{
		deleteShader(entry.first);
	}
}

bool ShaderManager::newShader(const std::string& name,
                              Shader* shader)
{
	LOG_IF(shaders.find(name) != shaders.end(),
	       LOG_WARN,
	       "Shader with the name '%s' already exists",
	       name.c_str());

	return shaders.emplace(name, shader).second;
}

Shader* ShaderManager::getShader(const std::string& name)
{
	auto shader = shaders.find(name);

	LOG_IF(shader == shaders.end(),
	       LOG_ERR,
	       "Could not find shader '%s'", name.c_str());

	return shader != shaders.end() ? shader->second : nullptr;
}

void ShaderManager::deleteShader(const std::string& name)
{
	auto entry = shaders.find(name);

	if(entry != shaders.end())
	{
		delete entry->second;
		shaders.erase(entry);
	}
}
}
