#include <samstorm/graphics/shaders/shader.hpp>
#include <samstorm/debug/logger.hpp>

namespace Samstorm
{
Shader::~Shader()
{
	glDeleteProgram(shader);
}

bool Shader::load(const std::string& vertexShaderSrc,
                  const std::string& fragmentShaderSrc)
{
	return createShader(vertexShaderSrc, fragmentShaderSrc);
}

bool Shader::load(const char* vertexShaderSrc,
                  const char* fragmentShaderSrc,
                  size_t vertexShaderSrcLen,
                  size_t fragmentShaderSrcLen)
{
	std::string vertexShader;
	std::string fragmentShader;

	if(vertexShaderSrcLen > 0 && fragmentShaderSrcLen > 0)
	{
		vertexShader.assign(vertexShaderSrc, vertexShaderSrcLen);
		fragmentShader.assign(fragmentShaderSrc, fragmentShaderSrcLen);
	}
	else
	{
		vertexShader.assign(vertexShaderSrc);
		fragmentShader.assign(fragmentShaderSrc);
	}

	return load(vertexShader, fragmentShader);
}

void Shader::use()
{
	glUseProgram(shader);
}

void Shader::preRender()
{
}

void Shader::postRender()
{
}

unsigned int Shader::getShader() const
{
	return shader;
}

GLint Shader::getUniformId(const std::string &uniformName) const
{
	auto uniform = uniformMap.find(uniformName);
	int uniformLocation;

	if(uniform == uniformMap.end())
	{
		uniformLocation = glGetUniformLocation(shader, uniformName.c_str());

		if(uniformLocation < 0)
		{
			LOG(LOG_ERR, "Could not find uniform '%s'", uniformName.c_str());
			return uniformLocation;
		}

		uniformMap.emplace(uniformName, uniformLocation);
	}
	else
	{
		uniformLocation = uniform->second;
	}

	return uniformLocation;
}

void Shader::setUniform1i(const std::string &name, const GLint value)
{
	glUniform1i(this->getUniformId(name.c_str()), value);
}

void Shader::setUniform3fv(const std::string &name, const GLfloat *value)
{
	glUniform3fv(this->getUniformId(name.c_str()), 1, value);
}

void Shader::setUniformMatrix4fv(const std::string &name, const GLfloat *value)
{
	glUniformMatrix4fv(this->getUniformId(name.c_str()), 1, GL_FALSE, value);
}

bool Shader::createShader(const std::string& vertexShaderSource, const std::string& fragmentShaderSource)
{
	shader = glCreateProgram();
	unsigned int vertexShader = compileShader(GL_VERTEX_SHADER, vertexShaderSource.c_str());
	unsigned int fragmentShader = compileShader(GL_FRAGMENT_SHADER, fragmentShaderSource.c_str());

	LOG_IF(vertexShader == 0 || fragmentShader == 0, LOG_ERR, "Shader creation failed");

	if(vertexShader == 0 || fragmentShader == 0)
		return false;

	glAttachShader(shader, vertexShader);
	glAttachShader(shader, fragmentShader);
	glLinkProgram(shader);

	glValidateProgram(shader);

	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);

	return true;
}

unsigned int Shader::compileShader(GLenum type, const char *src)
{
	unsigned int id = glCreateShader(type);
	glShaderSource(id, 1, &src, 0);
	glCompileShader(id);

	int result;

	glGetShaderiv(id, GL_COMPILE_STATUS, &result);

	if(!result)
	{
		int length;
		glGetShaderiv(id, GL_INFO_LOG_LENGTH, &length);
		char *message = (char*)alloca(length*sizeof(char));
		glGetShaderInfoLog(id, length, &length, message);

		std::string shaderType = (type == GL_VERTEX_SHADER) ? "vertex" : "fragment";
		LOG(LOG_ERR, "Failed to compile %s shader", shaderType.c_str());
		LOG(LOG_ERR, "Error message: %s", message);

		return 0;
	}

	return id;
}
}
