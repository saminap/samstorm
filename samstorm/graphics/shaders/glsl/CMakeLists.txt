include(helpers)

add_library(samstorm_graphics_shaders_glsl INTERFACE)

add_shader_dependency(samstorm_graphics_shaders_glsl
	INTERFACE
		color.vert
		color.frag
		texture.vert
		texture.frag
		font.vert
		font.frag
		light.vert
		light.frag
)
