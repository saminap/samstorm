#version 330 core

layout(location = 0) in vec4 position;
layout(location = 1) in vec2 textureCoordinate;
layout(location = 2) in vec4 color;

uniform mat4 u_mvp;

out vec2 v_texCoord;
out vec4 v_color;

void main()
{
	gl_Position = u_mvp * position;
	v_texCoord = textureCoordinate;
	v_color = color;
}
