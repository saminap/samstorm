#version 330 core

layout(location = 0) out vec4 color;

in vec2 v_texCoord;
in vec4 v_color;

uniform sampler2D u_texture;

void main()
{
	// Font color is stored in red channel and
	// border color in green channel

	vec4 texColor = texture(u_texture, v_texCoord);
	color = vec4(v_color.rgb, texColor.r*v_color.a + texColor.g*v_color.a);
}
