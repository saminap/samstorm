#version 400 core

layout(location = 0) out vec4 color;

in vec2 v_texCoord;
in vec4 v_color;

void main()
{
	float distance = length(v_texCoord);
	color = vec4(v_color.rgb, v_color.a*pow(0.01, distance)-0.01);
}
