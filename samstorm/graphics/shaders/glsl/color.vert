#version 400 core

layout(location = 0) in vec4 position;
layout(location = 2) in vec4 color;

uniform highp mat4 u_mvp;

out vec4 v_color;

void main()
{
	gl_Position = u_mvp * position;
	v_color = color;
}
