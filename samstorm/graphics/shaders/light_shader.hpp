#include <samstorm/graphics/shaders/shader.hpp>

namespace Samstorm
{
class LightShader : public Shader
{
public:
	LightShader(){}
	void preRender() override;
	void postRender() override;

private:
	GLint prevBlendFunc = 0;
};
}
