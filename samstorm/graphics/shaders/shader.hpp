#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h> 
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <map>

namespace Samstorm
{
class Shader
{

public:
	Shader(){}
	~Shader();
	bool load(const std::string& vertexShaderSrc,
	          const std::string& fragmentShaderSrc);
	bool load(const char* vertexShaderSrc,
	          const char* fragmentShaderSrc,
	          size_t vertexShaderSrcLen=0,
	          size_t fragmentShaderSrcLen=0);
	void use();
	virtual void preRender();
	virtual void postRender();
	unsigned int getShader() const;
	GLint getUniformId(const std::string &uniformName) const;
	void setUniform1i(const std::string &name, const GLint value);
	void setUniform3fv(const std::string &name, const GLfloat *value);
	void setUniformMatrix4fv(const std::string &name, const GLfloat *value);

private:
	unsigned int shader;
	mutable std::map<std::string, GLint> uniformMap;

	bool createShader(const std::string& vertexShaderSource, const std::string& fragmentShaderSource);
	unsigned int compileShader(GLenum type, const char *src);
};
}
