#pragma once

#include <samstorm/graphics/shaders/shader.hpp>
#include <samstorm/debug/logger.hpp>

#include <iostream>
#include <map>

namespace Samstorm
{
class ShaderManager
{
public:
	ShaderManager();
	~ShaderManager();

	template<typename T=Shader>
	bool newShader(const std::string& name,
	               const std::string& vertexShader,
	               const std::string& fragmentShader)
	{
		bool ret = true;
		Shader* shader = new T();

		if(shader->load(vertexShader, fragmentShader))
		{
			auto entry = shaders.emplace(name, shader);

			LOG(LOG_INFO, "succesfully created shader '%s'", name.c_str());

			// Shader with the name already exists
			if(!entry.second)
			{
				LOG(LOG_WARN,
				    "Shader with the name '%s' already exists",
				    name.c_str());

				ret = false;
			}
		}
		else
		{
			ret = false;
		}

		if(!ret)
			delete shader;

		return ret;
	}

	template<typename T=Shader>
	bool newShader(const std::string& name,
	               const char* vertexShaderStart,
	               const char* vertexShaderEnd,
	               const char* fragmentShaderStart,
	               const char* fragmentShaderEnd)
	{
		size_t vertexShaderLength = vertexShaderEnd-vertexShaderStart;
		size_t fragShaderLength = fragmentShaderEnd-fragmentShaderStart;

		std::string vertexShader{vertexShaderStart, vertexShaderLength};
		std::string fragShader{fragmentShaderStart, fragShaderLength};

		return newShader<T>(name, vertexShader, fragShader);
	}

	template<typename T=Shader>
	bool newShader(const std::string& name,
	               const char* vertexShader,
	               const char* fragmentShader,
	               size_t vertexShaderLength=0,
	               size_t fragmentShaderLength=0)
	{
		std::string vertex{vertexShader, vertexShaderLength};
		std::string frag{fragmentShader, fragmentShaderLength};

		return newShader<T>(name, vertex, frag);
	}

	bool newShader(const std::string& name,
	               Shader* shader);
	Shader* getShader(const std::string& name);
	void deleteShader(const std::string& name);

private:
	std::map<std::string, Shader*> shaders;
};
}
