#include <samstorm/graphics/renderer.hpp>
#include <samstorm/debug/logger.hpp>
#include <samstorm/debug/profiling.hpp>

#include <tuple>

namespace Samstorm
{
Renderer::Renderer(GLFWwindow* window)
 : window(window)
{
}

Renderer::~Renderer()
{
}

void Renderer::initVertexBuffer(const std::vector<VertexAttributeEntry>& vertexAttributes,
                                size_t maxSpriteCount)
{
	if(vertexAttributes.size() == 0)
	{
		LOG(LOG_WARN,
		    "trying to initialize vertex buffer without attributes, not doing anything");

		return;
	}

	unsigned int offset = 0;
	unsigned int index = 0;
	std::vector<unsigned int> indices;

	for(unsigned int i=0; i<maxSpriteCount; ++i)
	{
		indices.push_back(offset);
		indices.push_back(offset+1);
		indices.push_back(offset+2);
		indices.push_back(offset+2);
		indices.push_back(offset+3);
		indices.push_back(offset);

		offset += 4;
	}

	glGenVertexArrays(1, &opengl.vertexArray);
	bindVertexArray(opengl);

	glGenBuffers(1, &opengl.vertexBuffer);
	bindVertexBuffer(opengl);
	glBufferData(GL_ARRAY_BUFFER,
	             maxSpriteCount * vertexAttributes.at(0).stride * 4,
	             NULL,
	             GL_DYNAMIC_DRAW);

	for(auto& vertexAttribute : vertexAttributes)
	{
		glEnableVertexAttribArray(index);

		glVertexAttribPointer(index,
		                      vertexAttribute.size,
		                      vertexAttribute.type,
		                      GL_FALSE,
		                      vertexAttribute.stride,
		                      vertexAttribute.pointer);

		LOG(LOG_INFO,
		    "vertex attribute(%u): size=%u type=%u stride=%u ptr=%u",
		    index,
		    vertexAttribute.size,
		    vertexAttribute.type,
		    vertexAttribute.stride,
		    vertexAttribute.pointer);

		++index;
	}

	unbindVertexBuffer();

	glGenBuffers(1, &opengl.indexBuffer);
	bindIndexBuffer(opengl);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER,
	             indices.size()*sizeof(indices.at(0)),
	             &indices.at(0),
	             GL_DYNAMIC_DRAW);

	unbindIndexBuffer();

	renderingQueue.reserve(maxSpriteCount);

	LOG(LOG_INFO,
	    "renderer initialized with max sprite count=%u", maxSpriteCount);
}

void Renderer::bindVertexArray(OpenGLData& opengl) const
{
	glBindVertexArray(opengl.vertexArray);
}

void Renderer::bindVertexBuffer(OpenGLData& opengl) const
{
	glBindBuffer(GL_ARRAY_BUFFER, opengl.vertexBuffer);
}

void Renderer::bindIndexBuffer(OpenGLData& opengl) const
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, opengl.indexBuffer);
}

void Renderer::bindTexture(OpenGLData& opengl) const
{
	glActiveTexture(getTextureSlot(opengl));
	glBindTexture(GL_TEXTURE_2D, opengl.texture);
}

void Renderer::bindTexture(unsigned int textureId) const
{
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textureId);
}

void Renderer::unbindVertexArray() const
{
	glBindVertexArray(0);
}

void Renderer::unbindVertexBuffer() const
{
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void Renderer::unbindIndexBuffer() const
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void Renderer::unbindTexture(OpenGLData& opengl) const
{
	glActiveTexture(getTextureSlot(opengl));
	glBindTexture(GL_TEXTURE_2D, 0);
}

GLint Renderer::getTextureSlot(OpenGLData& opengl) const
{
	return GL_TEXTURE0 + opengl.textureSlot;
}

void Renderer::setProjectionViewMatrixUniform(Shader* shader, Camera* camera)
{
	const glm::mat4& mvp = camera != nullptr
	                               ? camera->getProjectionViewMatrix()
	                               : Camera::getProjectionMatrix();

	shader->setUniformMatrix4fv("u_mvp", &mvp[0][0]);
}

void Renderer::add(Drawable* drawable)
{
	renderingQueue.push_back(drawable);
}

void Renderer::clear()
{
	glClear(GL_COLOR_BUFFER_BIT);
}

void Renderer::begin()
{
	bindVertexArray(opengl);
	bindVertexBuffer(opengl);
	opengl.vertexBufferPointer = glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
	unbindVertexArray();
	queueOffset = 0;
}

void Renderer::flush()
{
	std::sort(renderingQueue.begin()+queueOffset,
	          renderingQueue.end(),
	          drawableComparator);

	queueOffset = renderingQueue.size();
}

void Renderer::render()
{
	PROFILE_THIS

	flush();

	bindVertexArray(opengl);
	bindVertexBuffer(opengl);

	for(auto& drawable : renderingQueue)
	{
		drawable->draw(&opengl.vertexBufferPointer);
	}

	glUnmapBuffer(GL_ARRAY_BUFFER);
	unbindVertexBuffer();
	bindIndexBuffer(opengl);

	unsigned int index = 0;
	unsigned int vertexBufferIndex = 0;

	while(index < renderingQueue.size())
	{
		Drawable* drawable = renderingQueue.at(index);
		Shader* shader = drawable->getShader();
		int count, vertexBufferEntryCount;
		std::tie(count, vertexBufferEntryCount) =
			sortedDrawQueueNextOffset(renderingQueue, index);
		GLuint textureId = drawable->getTextureId();

		if(textureId != 0)
		{
			bindTexture(textureId);
			shader->setUniform1i("u_texture", 0);
		}

		shader->use();
		setProjectionViewMatrixUniform(shader, drawable->getCamera());
		shader->preRender();

		glDrawElements(GL_TRIANGLES,
		               6*vertexBufferEntryCount,
		               GL_UNSIGNED_INT,
		               // @TODO: this is a bit hacky way to fix the compiler warning
		               //        'cast to pointer from integer of different size'
		               (const void*)((long unsigned int)vertexBufferIndex*4*6));

		shader->postRender();

		index += count;
		vertexBufferIndex += vertexBufferEntryCount;
	}

	queueOffset = 0;
	renderingQueue.clear();
}

void Renderer::swapBuffers()
{
	glfwSwapBuffers(window);
}

bool Renderer::drawableComparator(const Drawable* i, const Drawable* j)
{
	return
	       // 1st case: Z indices don't match
	       i->getZIndex() < j->getZIndex() ||

	       // 2nd case: Z indices match, sort by texture id
	       i->getZIndex() == j->getZIndex() && i->getTextureId() < j->getTextureId() ||

	       // 3rd case: Z indices and texture ids match, sort by shader
	       i->getZIndex()      == j->getZIndex() &&
	       i->getTextureId()   == j->getTextureId() &&
	       i->getShader()      <  j->getShader() ||

	       // 4th case: Z indices, texture ids and shaders match, sort by camera
	       i->getZIndex()      == j->getZIndex() &&
	       i->getTextureId()   == j->getTextureId() &&
	       i->getShader()      == j->getShader() &&
	       i->getCamera()      <  j->getCamera()

	       ;
}

std::tuple<int,int> Renderer::sortedDrawQueueNextOffset(const std::vector<Drawable*>& container,
                                                        int                           currentIndex) const
{
	int count = 0;
	int vertexBufferEntryCount = 0;

	if(currentIndex >= container.size())
		return std::make_tuple(count, vertexBufferEntryCount);

	Drawable* comp = container[currentIndex];

	for(; currentIndex < container.size(); ++currentIndex, ++count)
	{
		if(comp->getShader()    != container[currentIndex]->getShader()    ||
		   comp->getTextureId() != container[currentIndex]->getTextureId() ||
		   comp->getCamera()    != container[currentIndex]->getCamera())
			break;

		vertexBufferEntryCount += container[currentIndex]->vertexBufferEntryCount();
	}

	return std::make_tuple(count, vertexBufferEntryCount);
}
}
