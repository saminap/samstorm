#include <samstorm/graphics/animation.hpp>
#include <samstorm/io/resource_texture_mapper.hpp>

namespace Samstorm
{
Animation::Animation(v2<float>       coordinates,
                     v2<float>       size,
                     Shader*         shader,
                     const tmx_tile* tile,
                     Camera*         camera,
                     int             zIndex)
 : timeSinceUpdate(0)
{
	Drawable::move(coordinates);
	this->size = size;
	this->zIndex = zIndex;
	this->shader = shader;
	this->camera = camera;
	this->textureId = IO::getTextureId(tile->tileset);

	tmx_anim_frame* frame = tile->animation;
	if(frame)
	{
		for(unsigned int i=0; i<tile->animation_len; ++i, ++frame)
		{
			const tmx_tile* frameTile = tile->tileset->tiles+frame->tile_id;

			sprites.push_back(Sprite{
			    coordinates,
			    size,
			    frameTile,
			    shader,
			    camera,
			    zIndex
			});

			// Durations are represented as unsigned int milliseconds
			frameDurations.push_back((float)frame->duration/1000.0f);
		}
	}

	currentFrame = sprites.begin();
	frameDurationIter = frameDurations.begin();
}

void Animation::draw(void** vertexBufferPointer) const
{
	currentFrame->draw(vertexBufferPointer);
}

void Animation::update(double delta)
{
	timeSinceUpdate += delta;

	if(timeSinceUpdate >= *frameDurationIter)
	{
		timeSinceUpdate -= *frameDurationIter;

		if(++currentFrame == sprites.end())
			currentFrame = sprites.begin();

		if(++frameDurationIter == frameDurations.end())
			frameDurationIter = frameDurations.begin();
	}
}

void Animation::move(v2<float> to)
{
	Drawable::move(to);

	for(auto& frame : sprites)
	{
		frame.move(to);
	}
}
}
