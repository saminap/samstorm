#include <samstorm/graphics/camera.hpp>

namespace Samstorm
{
v2<float> Camera::projectionSize = {0,0};
v2<float> Camera::diffToMonitor = {0,0};
glm::mat4 Camera::projectionMatrix;

Camera::Camera()
 : cameraCoordinates({0.0f, 0.0f}),
   followedCoordinates(nullptr),
   coordinatesOffset({0.0f, 0.0f}),
   followSpeed(7.5f)
{
}

void Camera::update(double delta)
{
	if(followedCoordinates)
	{
		const float closingSpeed = 1.5f; // @TODO: think about this again at some point
		float interpolation = followSpeed*delta;
		v2<float> target = {
			projectionSize.x/2.0f-followedCoordinates->x-coordinatesOffset.x,
			projectionSize.y/2.0f-followedCoordinates->y-coordinatesOffset.y
		};

		cameraCoordinates.x = glm::lerp<float>(cameraCoordinates.x, target.x, interpolation);
		cameraCoordinates.y = glm::lerp<float>(cameraCoordinates.y, target.y, interpolation);

		v2<float> diff = {
			std::abs(target.x-cameraCoordinates.x),
			std::abs(target.y-cameraCoordinates.y)
		};

		if(diff.x < 1.5f)
			cameraCoordinates.x = glm::lerp<float>(cameraCoordinates.x,
			                                       target.x,
			                                       interpolation*closingSpeed);

		if(diff.y < 1.5f)
			cameraCoordinates.y = glm::lerp<float>(cameraCoordinates.y,
			                                       target.y,
			                                       interpolation*closingSpeed);
	}

	viewMatrix = glm::translate(IDENTITY_MATRIX, glm::vec3(cameraCoordinates.x,
		                                               cameraCoordinates.y,
		                                               0.0f));

	glm::mat4 scalingMatrix = glm::scale(IDENTITY_MATRIX, glm::vec3(zoom, zoom, 0.0f));
	viewMatrix *= scalingMatrix;

	projectionViewMatrix = getProjectionMatrix() * viewMatrix;
}

v2<float> Camera::getCoordinates() const
{
	return cameraCoordinates;
}

void Camera::setCoordinates(const v2<float>& coordinates)
{
	cameraCoordinates = {coordinates.x, coordinates.y};
}

void Camera::followCoordinates(const v2<float>* newCoordinates, v2<float> objectSize)
{
	cameraCoordinates = {newCoordinates->x, newCoordinates->y};
	followedCoordinates = newCoordinates;
	coordinatesOffset.x = objectSize.x/2.0f;
	coordinatesOffset.y = objectSize.y/2.0f;
}

void Camera::unfollowCoordinates()
{
	followedCoordinates = nullptr;
}

const glm::mat4& Camera::getProjectionViewMatrix() const
{
	return projectionViewMatrix;
}

const glm::mat4& Camera::getViewMatrix() const
{
	return viewMatrix;
}

const glm::mat4& Camera::getProjectionMatrix()
{
	return projectionMatrix;
}

void Camera::setProjectionSize(const v2<float>& size)
{
	projectionSize = size;
	projectionMatrix = glm::ortho(0.0f,
	                              projectionSize.x,
	                              projectionSize.y,
	                              0.0f);
}

void Camera::setDiffToMonitor(const v2<float>& diff)
{
	diffToMonitor = diff;
}

const v2<float>& Camera::getProjectionSize()
{
	return projectionSize;
}

const v2<float>& Camera::getDiffToMonitor()
{
	return diffToMonitor;
}
}
