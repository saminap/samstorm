#include <samstorm/graphics/text.hpp>
#include <samstorm/debug/logger.hpp>
#include <samstorm/io/resource_texture_mapper.hpp>

namespace Samstorm
{
Text::Text(const std::string& text,
           const v2<float>&   coordinates,
           const Color&       textColor,
           const Color&       borderColor,
           Shader*            shader,
           FTFont::Font*      font,
           Camera*            camera,
           unsigned int       borderSize,
           float              scale,
           int                zIndex)
 : borderColor(borderColor),
   text(text),
   font(font),
   borderSize(borderSize),
   scale(1.f/Camera::getDiffToMonitor().y*scale),
   initialCoordinates(coordinates)
{
	this->size.x = 0;
	this->size.y = 0;
	this->coordinates = coordinates;
	this->color = textColor;
	this->zIndex = zIndex;
	this->shader = shader;
	this->textureId = IO::getTextureId(font);
	this->camera = camera;

	updateText(text);
}

void Text::draw(void** vertexBufferPointer) const
{
	for(auto& c : borders)
	{
		c.draw(vertexBufferPointer);
	}

	for(auto& c : characters)
	{
		c.draw(vertexBufferPointer);
	}
}

void Text::move(v2<float> to)
{
	Drawable::move(to);

	if(characters.size())
	{
		v2<float> first = borders.empty() ? characters.begin()->getCoordinates() :
		                                    borders.begin()->getCoordinates();

		for(Drawable& drawable : characters)
		{
			v2<float> differenceToFirst = drawable.getCoordinates()-first;
			v2<float> target = to+differenceToFirst;
			drawable.move(target);
		}

		for(Drawable& drawable : borders)
		{
			v2<float> differenceToFirst = drawable.getCoordinates()-first;
			v2<float> target = to+differenceToFirst;
			drawable.move(target);
		}
	}
}

void Text::setColor(const Color& color)
{
	// @TODO: Separate method for setting border color needs to be implemented
	for(Drawable& drawable : characters)
	{
		drawable.setColor(color);
	}
}

void Text::updateText(const std::string& newText)
{
	text.clear();
	characters.clear();
	borders.clear();

	append(newText);
}

void Text::append(const std::string& appendedText)
{
	v2<size_t> atlasSize = {font->getAtlasWidth(), font->getAtlasHeight()};
	float advance = 0;
	int index = text.length();

	// In case the existing text is not empty, we need to calculate what the
	// advance was at the end of the existing text
	for(auto& c : text)
		advance += font->getChar(c, borderSize)->advance;

	advance *= scale;
	text += appendedText;

	characters.resize(text.length());

	if(borderSize > 0)
		borders.resize(text.length());

	for(char c : appendedText)
	{
		LOG_IF(c < FTFont::Font::ASCII_START || c > FTFont::Font::ASCII_END,
		       LOG_WARN,
		       "trying to append a non-ASCII character, code=0x%hhx", c);

		const FTFont::Char* character = font->getChar(c);
		const FTFont::Char* border = font->getChar(c, borderSize);

		v2<float> atlasCoordinates = {
			(float)character->x/(float)atlasSize.x,
			(float)character->y/(float)atlasSize.y
		};
		v2<float> textureSize = {
			(float)character->w/(float)atlasSize.x,
			(float)character->h/(float)atlasSize.y
		};
		v2<float> diffToBorder = {
			(float)(border->w-character->w)/2.0f*scale,
			(float)(border->h-character->h)/2.0f*scale
		};

		characters.at(index) = Drawable{
			this->coordinates.x+advance+diffToBorder.x+border->bearing*scale,
			this->coordinates.y+diffToBorder.y,
			character->w*scale,
			character->h*scale,
			color,
			atlasCoordinates,
			textureSize,
			shader,
			textureId,
			camera,
			zIndex+1 // Render the border behind the text
		};

		if(borderSize > 0)
		{
			v2<float> borderAtlasCoordinates = {
				(float)border->x/(float)atlasSize.x,
				(float)border->y/(float)atlasSize.y
			};
			v2<float> borderTextureSize = {
				(float)border->w/(float)atlasSize.x,
				(float)border->h/(float)atlasSize.y
			};

			borders.at(index) = {
				this->coordinates.x+advance+border->bearing*scale,
				this->coordinates.y,
				border->w*scale,
				border->h*scale,
				borderColor,
				borderAtlasCoordinates,
				borderTextureSize,
				shader,
				textureId,
				camera,
				zIndex
			};
		}

		advance += border->advance*scale;
		++index;
	}

	calculateSize();
}

void Text::pop()
{
	if(text.empty())
		return;

	characters.pop_back();
	text.pop_back();

	if(borderSize)
		borders.pop_back();

	calculateSize();
}

const std::string& Text::getText() const
{
	return text;
}

const Color& Text::getBorderColor() const
{
	return borderColor;
}

unsigned int Text::getBorderSize() const
{
	return borderSize;
}

const FTFont::Font* Text::getFont() const
{
	return font;
}

void Text::calculateSize()
{
	std::vector<Drawable>::const_iterator iter = borders.empty() ? characters.begin() :
	                                                               borders.begin();
	std::vector<Drawable>::const_iterator end = borders.empty() ? characters.end() :
	                                                              borders.end();
	size.x = 0;
	size.y = 0;

	if(text.empty())
		return;

	std::vector<Drawable>::const_iterator last = std::prev(end);
	size.x = last->getCoordinates().x + last->getSize().x - iter->getCoordinates().x;

	for(; iter != end; ++iter)
		size.y = std::max(size.y, iter->getSize().y);
}

int Text::vertexBufferEntryCount() const
{
	return borderSize > 0 ? text.size()*2 : text.size();
}
}
