#pragma once

#include <samstorm/misc/common.hpp>
#include <samstorm/graphics/shaders/shader.hpp>
#include <samstorm/graphics/camera.hpp>

extern "C" {
#include <samstorm/3rdparty/tmx/tmx.h>
}

namespace Samstorm
{
class Drawable
{

public:
	Drawable(){}

	Drawable(const float x,
	         const float y,
	         const float width,
	         const float height,
	         const Color color,
	         v2<float>   textureCoordinates,
	         v2<float>   textureSize,
	         Shader*     shader,
	         GLuint      textureId=0,
	         Camera*     camera=nullptr,
	         int         zIndex=0,
	         float       rotation=0.0f);

	Drawable(v2<float> coordinates,
	         v2<float> size,
	         const Color color,
	         v2<float> textureCoordinates,
	         v2<float> textureSize,
	         Shader*   shader,
	         GLuint    textureId=0,
	         Camera*   camera=nullptr,
	         int       zIndex=0,
	         float     rotation=0.0f);

	const v2<float>& getCoordinates() const;
	const v2<float>& getSize() const;
	virtual const Color& getColor() const;
	virtual void setColor(const Color& color);
	const v2<float> getTextureAtlasCoordinates() const;
	const float getRotation() const;
	const v2<float>& getCenterOfRotation() const;
	virtual const v2<float> getTextureSize() const;
	virtual void draw(void** vertexBufferPointer) const;
	virtual void move(v2<float> to);
	void setRotation(float rotation);
	void setCenterOfRotation(const v2<float>& point);
	void setZIndex(int zIndex);
	int getZIndex() const;
	GLuint getTextureId() const;
	void setTextureId(GLuint texture);
	void setShader(Shader* shader);
	Shader* getShader() const;
	Camera* getCamera() const;
	virtual int vertexBufferEntryCount() const;
	virtual void flipHorizontal();
	virtual void flipVertical();

protected:
	v2<float> coordinates;
	v2<float> textureCoordinates;
	int zIndex;
	GLuint textureId;
	float rotation;
	v2<float> size;
	v2<float> textureSize;
	Color color;
	unsigned int drawableType;
	Shader* shader;
	Camera* camera;
	v2<float> centerOfRotation;
	float sinrot;
	float cosrot;
};
}
