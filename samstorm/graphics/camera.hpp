#pragma once

#define GLM_ENABLE_EXPERIMENTAL

#include <samstorm/misc/common.hpp>
#include <samstorm/3rdparty/glm/gtx/compatibility.hpp>

namespace Samstorm
{
class Camera
{
public:
	Camera();
	virtual void update(double delta);
	v2<float> getCoordinates() const;
	void setCoordinates(const v2<float>& coordinates);
	void followCoordinates(const v2<float>* newCoordinates,
	                       v2<float> objectSize={0.0f, 0.0f});
	void unfollowCoordinates();
	const glm::mat4& getProjectionViewMatrix() const;
	const glm::mat4& getViewMatrix() const;
	static const glm::mat4& getProjectionMatrix();
	static void setProjectionSize(const v2<float>& size);
	static void setDiffToMonitor(const v2<float>& diff);
	static const v2<float>& getProjectionSize();
	static const v2<float>& getDiffToMonitor();

protected:
	float zoom = 1.0f;
	v2<float> cameraCoordinates;
	const v2<float>* followedCoordinates;
	v2<float> coordinatesOffset;
	float followSpeed;
	static v2<float> projectionSize;
	static v2<float> diffToMonitor;
	static glm::mat4 projectionMatrix;
	glm::mat4 viewMatrix;
	glm::mat4 projectionViewMatrix;
};
}
