#pragma once

#include <samstorm/misc/common.hpp>
#include <samstorm/graphics/drawable.hpp>
#include <samstorm/graphics/text.hpp>
#include <samstorm/graphics/light.hpp>
#include <samstorm/graphics/shaders/shader.hpp>
#include <samstorm/graphics/camera.hpp>
#include <samstorm/3rdparty/glm/mat4x4.hpp>
#include <samstorm/3rdparty/glm/gtc/matrix_transform.hpp>
#include <samstorm/3rdparty/freetype-font/freetype_font.hpp>
#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_STROKER_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <map>
#include <utility>
#include <algorithm>

namespace Samstorm
{
class Renderer
{
public:
	Renderer(GLFWwindow* window);
	~Renderer();
	void initVertexBuffer(const std::vector<VertexAttributeEntry>& vertexAttributes,
	                      size_t maxSpriteCount=GL_SPRITE_COUNT);
	void add(Drawable* drawable);
	void clear();
	void begin();
	void flush();
	void render();
	void swapBuffers();

private:
	GLFWwindow* window;
	std::vector<Drawable*> renderingQueue;
	OpenGLData             opengl;
	unsigned int           queueOffset{0};

	static inline bool drawableComparator(const Drawable* i, const Drawable* j);

	std::tuple<int,int> sortedDrawQueueNextOffset(const std::vector<Drawable*>& container,
	                                              int                           current) const;

	void setProjectionViewMatrixUniform(Shader* shader, Camera* camera);
	void bindVertexArray(OpenGLData& opengl) const;
	void bindVertexBuffer(OpenGLData& opengl) const;
	void bindIndexBuffer(OpenGLData& opengl) const;
	void bindTexture(OpenGLData& opengl) const;
	void bindTexture(unsigned int textureId) const;
	void unbindVertexArray() const;
	void unbindVertexBuffer() const;
	void unbindIndexBuffer() const;
	void unbindTexture(OpenGLData& opengl) const;
	GLint getTextureSlot(OpenGLData& opengl) const;
};
}
