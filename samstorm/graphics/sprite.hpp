#pragma once

#include <samstorm/misc/common.hpp>
#include <samstorm/graphics/shaders/shader.hpp>
#include <samstorm/graphics/drawable.hpp>

namespace Samstorm
{
class Sprite : public Drawable
{
public:
	Sprite(v2<float>       coordinates,
	       v2<float>       size,
	       const tmx_tile* tile,
	       Shader*         shader,
	       Camera*         camera=nullptr,
	       int             zIndex=0,
	       float           rotation=0.0f);
};
}

