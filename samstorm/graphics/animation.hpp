#pragma once

#include <samstorm/graphics/shaders/shader.hpp>
#include <samstorm/graphics/sprite.hpp>

namespace Samstorm
{
class Animation : public Drawable
{
public:
	Animation(v2<float>       coordinates,
	          v2<float>       size,
	          Shader*         shader,
	          const tmx_tile* tile,
	          Camera*         camera=nullptr,
	          int             zIndex=0);

	virtual void draw(void** vertexBufferPointer) const override;
	void update(double delta);
	void move(v2<float> to) override;

private:
	double timeSinceUpdate;
	std::vector<Sprite> sprites;
	std::vector<Sprite>::iterator currentFrame;
	std::vector<float> frameDurations;
	std::vector<float>::iterator frameDurationIter;
};
}
