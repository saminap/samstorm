#pragma once

#include <samstorm/graphics/drawable.hpp>
#include <samstorm/graphics/shaders/shader.hpp>
#include <samstorm/misc/common.hpp>

namespace Samstorm
{
class Line : public Drawable
{
public:
	Line();
	Line(const v2<float>& start,
	     const v2<float>& end,
	     float width,
	     const Color& color,
	     Shader* shader,
	     Camera* camera=nullptr,
	     int zIndex=0);

private:
};
}
