#include <samstorm/debug/profiling_manager.hpp>
#include <samstorm/graphics/text.hpp>
#include <samstorm/debug/console.hpp>

namespace Samstorm
{
namespace Internal
{
namespace ProfilingManager
{
namespace
{
std::unordered_map<std::string, ProfilingEntry> results;
std::vector<Text> texts;
Shader* shader_;
FTFont::Font* font_;
bool display = false;
int updatePeriod = 0;
int updateCounter = 0;

void toggleProfiling()
{
	display = !display;
}

void profilingUpdatePeriod(const std::string& cmd,
                           std::vector<std::string> params)
{
	if(!params.empty())
	{
		updatePeriod = std::stoi(params.back());
	}
}
}
void init(Shader* shader, FTFont::Font* font)
{
	using namespace std::placeholders;

	shader_  = shader;
	font_ = font;

	Console::registerListener("toggle_profiling", std::bind(&toggleProfiling));
	Console::registerListener("profiling_update_period",
	                          std::bind(&profilingUpdatePeriod, _1, _2));
}

void addResult(const std::string& name, double duration)
{
	ProfilingEntry& entry = results[name];

	entry.max = std::max(entry.max, duration);
	entry.min = std::min(entry.min, duration);
	entry.totalDuration += duration;
	entry.last = duration;
	++entry.count;
	entry.average = entry.totalDuration / static_cast<double>(entry.count);
}

const std::unordered_map<std::string, ProfilingEntry>& getResults()
{
	return results;
}

void render(Renderer* renderer)
{
	if(!display)
		return;

	if(updatePeriod > 0)
		++updateCounter;

	if(updateCounter >= updatePeriod)
	{
		updateCounter = 0;

		texts.clear();

		for(auto result : results)
		{
			std::ostringstream stream;
			stream << std::fixed << std::setprecision(2)
			       << "max=" << result.second.max << "ms "
			       << "min=" << result.second.min << "ms "
			       << "total=" << result.second.totalDuration << "ms "
			       << "last=" << result.second.last << "ms "
			       << "avg=" << result.second.average << "ms "
			       << "cnt=" << result.second.count;

			texts.push_back(Text{
				result.first + ": " + stream.str(),
				{0, 0},
				{1, 1, 1, 1},
				{0, 0, 0, 0},
				shader_,
				font_
			});

			if(texts.size() > 1)
			{
				auto prev = texts.rbegin()+1;
				texts.back().move({
					0,
					prev->getCoordinates().y + prev->getSize().y
				});
			}
		}
	}

	for(Text& text : texts)
	{
		renderer->add(&text);
	}
}
}
}
}
