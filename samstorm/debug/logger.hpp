#pragma once

#include <iostream>
#include <samstorm/debug/console.hpp>

namespace Samstorm
{
namespace Logger
{
	static std::ostream& output = std::cout;
	static void logToFile(const std::string& filename="samstorm.log")
	{
		static std::ofstream buf{filename, std::ios::out};
		output.rdbuf(buf.rdbuf());
	}
}
}

#ifndef NDEBUG

// log level, source file, line number, function name, log message
#define LOG_FORMAT "[%-4s] %s (%d): %s(): %s"

namespace Samstorm
{
namespace Internal
{
template<typename... Args>
static void log(const char* level,
                std::string file,
                int         line,
                const char* func,
                const char* fmt,
                Args...     args)
{
	// @TODO: increase the buffer sizes if these are not enough
	char msg[300] = {0};

	// Remove the preceding ../ from the file path
	while(!file.empty() && (file.front() == '.' || file.front() == '/'))
		file.erase(0, 1);

	if(sizeof...(Args) > 0)
	{
		char buf[200] = {0};
		snprintf(buf, sizeof(buf), fmt, args...);
		snprintf(msg, sizeof(msg), LOG_FORMAT, level, file.c_str(), line, func, buf);
	}
	else
	{
		snprintf(msg, sizeof(msg), LOG_FORMAT, level, file.c_str(), line, func, fmt);
	}

	Samstorm::Logger::output << msg << std::endl;
	Samstorm::Console::print(std::string(msg));
}
}
}

#define LOG_INFO "INFO"
#define LOG_WARN "WARN"
#define LOG_ERR  "ERR"

#define LOG(level, fmt, ...) \
	::Samstorm::Internal::log(level, __FILE__, __LINE__, __FUNCTION__, fmt, ##__VA_ARGS__);

#define LOG_IF(condition, level, fmt, ...) \
	if(condition) LOG(level, fmt, ##__VA_ARGS__);
#else
#define LOG(level, msg, ...)
#define LOG_IF(condition, level, msg, ...)
#endif
