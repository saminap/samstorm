#pragma once

#include <samstorm/graphics/text.hpp>
#include <samstorm/graphics/renderer.hpp>

#include <iostream>
#include <vector>
#include <deque>
#include <string>
#include <memory>
#include <functional>

namespace Samstorm
{
namespace Console
{
void init(Shader* colorShader,
          Shader* fontShader,
          FTFont::Font* consoleFont);
void update(double delta);
void render(Renderer& renderer);
void print(const std::string& text);
void registerListener(const std::string& key,
                      std::function<void(std::string, std::vector<std::string>)> callback);
}
}
