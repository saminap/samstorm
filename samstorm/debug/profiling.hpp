#pragma once

#include <samstorm/debug/profiling_manager.hpp>
#include <samstorm/debug/profiling_timer.hpp>

#include <string>

namespace Samstorm
{
namespace Internal
{
inline std::string constructProfilerName(const char* file,
                                         int line,
                                         const char* func)
{
	std::string fileName{file};

	while(!fileName.empty() && (fileName.front() == '.' || fileName.front() == '/'))
		fileName.erase(0, 1);

	return std::string{
		fileName +
		"(" + std::to_string(line) + "): " +
		std::string(func) + "()"};
}
}
#ifndef NDEBUG
#define PROFILE_THIS \
	Internal::ProfilingTimer profilingTimer##__LINE__(Internal::constructProfilerName(__FILE__, __LINE__, __FUNCTION__));
#else
#define PROFILE_THIS
#endif
}
