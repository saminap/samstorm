#pragma once

#include <chrono>
#include <string>

namespace Samstorm
{
namespace Internal
{
class ProfilingTimer
{
public:
	ProfilingTimer(const std::string& name);
	~ProfilingTimer();

private:
	std::string name;
	std::chrono::high_resolution_clock::time_point start;
};
}
}
