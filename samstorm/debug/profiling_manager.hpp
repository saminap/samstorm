#pragma once

#include <samstorm/graphics/renderer.hpp>
#include <samstorm/graphics/shaders/shader.hpp>

#include <string>
#include <unordered_map>
#include <limits>

namespace Samstorm
{
namespace Internal
{
namespace ProfilingManager
{
typedef struct
{
	double max=0;
	double min=std::numeric_limits<double>::max();
	double totalDuration=0;
	double last=0;
	double average=0;

	// @TODO: uint32_t should be enough if we are executing
	//        at 60fps. Keep this in mind if funny things
	//        start to happen when executing with uncapped fps.
	uint32_t count=0;
}ProfilingEntry;

void init(Shader* shader, FTFont::Font* font);
void addResult(const std::string& name, double duration);
const std::unordered_map<std::string, ProfilingEntry>& getResults();
void render(Renderer* renderer);
}
}
}
