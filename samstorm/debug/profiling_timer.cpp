#include <samstorm/debug/profiling_timer.hpp>
#include <samstorm/debug/profiling_manager.hpp>
#include <samstorm/debug/logger.hpp>

namespace Samstorm
{
namespace Internal
{
ProfilingTimer::ProfilingTimer(const std::string& name)
 : name(name),
   start(std::chrono::high_resolution_clock::now())
{
}

ProfilingTimer::~ProfilingTimer()
{
	std::chrono::high_resolution_clock::time_point end =
		std::chrono::high_resolution_clock::now();

	long long startTimePoint = std::chrono::time_point_cast<std::chrono::microseconds>(
		start).time_since_epoch().count();

	long long endTimePoint = std::chrono::time_point_cast<std::chrono::microseconds>(
		end).time_since_epoch().count();

	ProfilingManager::addResult(
		name,
		(endTimePoint - startTimePoint) * 0.001f);
}
}
}
