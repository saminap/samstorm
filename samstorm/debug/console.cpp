#include <samstorm/debug/console.hpp>
#include <samstorm/debug/logger.hpp>
#include <samstorm/debug/profiling_manager.hpp>
#include <samstorm/events/key_event.hpp>
#include <samstorm/events/text_input_event.hpp>
#include <samstorm/events/mouse_scroll_event.hpp>
#include <samstorm/events/event_manager.hpp>
#include <samstorm/events/event_listener.hpp>

#include <iterator>

namespace Samstorm
{
namespace Console
{
namespace
{
std::deque<std::string> history;
int historyIndex = -1;
const Color backgroundColor{0.1f, 0.1f, 0.1f, .75f};
Drawable background;
std::vector<Text> consoleLines;
bool scrolled = false;
bool consoleHeightCalculated = false;
int32_t scrollOffset = 0;
int32_t scrollSpeed = 3;
int32_t consolePrintCount = 0;
float consoleFontScale = 1.0f;
Camera scrollCamera;
std::vector<std::string> earlyConsolePrints;
const std::string defaultPrompt{"> "};
const std::string defaultPromptSuffix{"_"};
std::unique_ptr<Text> prompt;
std::string command{""};
bool consoleOpen{false};
Shader* colorShader_, *fontShader_;
FTFont::Font* consoleFont_;
std::map<std::string, std::function<void(std::string, std::vector<std::string>)>> listeners;
std::shared_ptr<EventListener> textInputEventHandle;
std::shared_ptr<EventListener> keyEventHandle;
std::shared_ptr<EventListener> mouseScrollHandle;
Event::CallbackFn textInputCallbackFunc;
Event::CallbackFn keyEventCallbackFunc;
// @TODO: parametrize this
const int consoleTrigger = 96; // ASCII

void help()
{
	print("------------------------");
	print("Available commands:");
	for(auto listener : listeners)
	{
		print(listener.first);
	}
	print("------------------------");
}

void setPrompt(const std::string& text)
{
	prompt->updateText(defaultPrompt);
	prompt->append(text);
	prompt->append(defaultPromptSuffix);
}

void evaluate()
{
	if(command.empty())
		return;

	std::stringstream stream(command);
	std::istream_iterator<std::string> begin(stream);
	std::istream_iterator<std::string> end;
	std::vector<std::string> params(begin, end);
	std::string key = params.at(0);

	params.erase(params.begin());
	history.push_front(command);

	if(listeners.find(key) == listeners.end())
	{
		print("Could not find command: " + command);
		return;
	}

	print(command);
	listeners[key](key, params);
}

std::string findCommonPrefix(const std::string& one, const std::string& two)
{
	std::string result{""};

	for(int32_t i=0; i<one.size() && i<two.size(); ++i)
	{
		if(one.at(i) == two.at(i))
			result.push_back(one.at(i));
		else
			break;
	}

	return result;
}

void textInputCallback(Event* event)
{
	TextInputEvent* e = (TextInputEvent*)event;
	int key = e->getKey();

	e->consume();

	if(key >= 32 && key <= 126)
	{
		std::stringstream ss;
		ss << (char)key;

		command.append(ss.str());
	}
	else if(key == 265) // Arrow up
	{
		if(!history.empty())
		{
			historyIndex = std::min(++historyIndex,
			                        (int)history.size()-1);

			command = history.at(historyIndex);
		}
	}
	else if(key == 264) // Arrow down
	{
		if(!history.empty())
		{
			historyIndex = std::max(--historyIndex, -1);

			if(historyIndex < 0)
				command = "";
			else
				command = history.at(historyIndex);
		}
	}
	else if(key == 8)
	{
		if(!command.empty())
			command.pop_back();
	}
	else if(key == 10)
	{
		evaluate();
		command = "";
		historyIndex = -1;
	}
	else if(key == 9) // Tab
	{
		auto iter = listeners.lower_bound(command);
		if(iter != listeners.end())
		{
			std::string key = iter->first;
			std::vector<std::string> found;

			while(key.compare(0, command.size(), command) == 0)
			{
				found.push_back(key);
				if(++iter == listeners.end())
					break;

				key = iter->first;
			}

			if(!found.empty())
			{
				if(found.size() > 1)
				{
					std::string result;
					for(auto c : found)
						result.append(c + "   ");

					print(result);
					command = findCommonPrefix(found.front(),
					                           found.back());
				}
				else
				{
					command = found.at(0);
				}
			}
		}
	}

	setPrompt(command);
}

void keyEventCallback(Event* event)
{
	KeyEvent* e = (KeyEvent*)event;

	if(consoleOpen)
		e->consume();

	if(e->getKey() == consoleTrigger && e->pressed())
	{
		if(!consoleOpen)
		{
			textInputEventHandle =
			    EventManager::addListener<TextInputEvent>(textInputCallbackFunc);
		}
		else
		{
			EventManager::removeListener(textInputEventHandle);
		}

		consoleOpen = !consoleOpen;
	}
}
}

void init(Shader* colorShader,
          Shader* fontShader,
          FTFont::Font* consoleFont)
{
	colorShader_ = colorShader;
	fontShader_ = fontShader;
	consoleFont_ = consoleFont;

	background = Drawable{
		{0, 0},
		{Camera::getProjectionSize().x, Camera::getProjectionSize().y},
	        backgroundColor,
		{0, 0},
		{0, 0},
		colorShader_
	};

	scrollCamera.setCoordinates({0, 0});
	scrollCamera.update(0);

	prompt.reset(new Text{
		"",
		{0, 0},
		{1, 1, 1, 1},
		{0, 0, 0, 1},
		fontShader_,
		consoleFont_,
		nullptr,
		0,
		1.f,
		1
	});

	setPrompt("");
	prompt->move(
	{
		0,
		Camera::getProjectionSize().y -
			consoleFont_->getMaxCharHeight() /
			Camera::getDiffToMonitor().y
	});

	for(auto& line : earlyConsolePrints)
		print(line);

	earlyConsolePrints.clear();
	earlyConsolePrints.shrink_to_fit();

	registerListener("help", std::bind(&help));

	textInputCallbackFunc = std::bind(&textInputCallback, std::placeholders::_1);
	keyEventCallbackFunc = std::bind(&keyEventCallback, std::placeholders::_1);

	keyEventHandle = EventManager::addListener<KeyEvent>(keyEventCallbackFunc,
	                                                     false);

	mouseScrollHandle = EventManager::addListener<MouseScrollEvent>(
	[](Event* event)
	{
		MouseScrollEvent* e = (MouseScrollEvent*)event;
		if(consoleHeightCalculated)
		{
			int32_t offset = (int32_t)e->getOffset().y * scrollSpeed;
			scrollOffset -= offset;
			scrollOffset = std::max(0, scrollOffset);
			scrollOffset = std::min((size_t)scrollOffset,
			                        consoleLines.size()-consolePrintCount);
			scrollCamera.setCoordinates(
			{
				0,
				-consoleLines.at(scrollOffset).getCoordinates().y
			});
			scrollCamera.update(0);

			if(scrollOffset == consoleLines.size()-consolePrintCount)
				scrolled = false;
			else
				scrolled = true;

		}

		if(consoleOpen)
			e->consume();
	});
}

void update(double delta)
{
}

void render(Renderer& renderer)
{
	if(!consoleOpen)
		return;

	renderer.flush();

	renderer.add(&background);
	renderer.add(prompt.get());

	for(int32_t i=0; i<consolePrintCount; ++i)
		renderer.add(&consoleLines[i+scrollOffset]);
}

void print(const std::string& text)
{
	if(fontShader_ == nullptr || colorShader_ == nullptr)
	{
		earlyConsolePrints.push_back(text);
		return;
	}

	float yOffset = 0;
	float projectionHeight = Camera::getProjectionSize().y;

	if(!consoleLines.empty())
	{
		Text& last = consoleLines.back();
		yOffset += last.getCoordinates().y +
			consoleFont_->getMaxCharHeight() /
			Camera::getDiffToMonitor().y*consoleFontScale;
	}

	consoleLines.push_back(Text{
		text,
		{0, yOffset},
		{1, 1, 1, 1},
		{0, 0, 0, 1},
		fontShader_,
		consoleFont_,
		&scrollCamera,
		0,
		consoleFontScale,
		1
	});

	Text& last = consoleLines.back();

	if(!scrolled)
	{
		scrollCamera.setCoordinates(
		{
			0,
			projectionHeight -
				last.getCoordinates().y -
				last.getSize().y -
				prompt->getSize().y
		});
		scrollCamera.update(0);
	}

	if(!scrolled && scrollCamera.getCoordinates().y < 0)
		consoleHeightCalculated = true;

	if(!consoleHeightCalculated)
	{
		++consolePrintCount;
	}
	else
	{
		if(!scrolled)
			++scrollOffset;
	}
}

void registerListener(const std::string& key,
                      std::function<void(std::string, std::vector<std::string>)> callback)
{
	LOG_IF(listeners.find(key) != listeners.end(),
	       LOG_WARN,
	       "Overriding callback for: %s", key.c_str());

	listeners[key] = callback;
}
}
}
