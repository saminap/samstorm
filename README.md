## Samstorm

Very simple OpenGL game engine project for educational purposes.

## Building

Before building, make sure to update git submodules with `git submodule update --init`. CMake is used to create the makefiles that build the engine. You may use the dockerfile located under docker folder to create a build environment for yourself. See [docker/README.md](docker/README.md) for further details.

#### Dependencies
- GLFW
- GLEW
- stb (source included in this repo)
- GLM (source included in this repo)
- JsonCpp (source included in this repo)
- FreeType
- freealut
- OpenAL-soft
- libogg
- libvorbis
- Very simple Tar lib that I wrote (https://bitbucket.org/saminap/simple-tar-lib) (included as Git submodule)
- FreeType library wrapper that I wrote (https://bitbucket.org/saminap/freetype-font) (included as Git submodule)

#### How to build

In case you are familiar with CMake, the usual workflow also works here. First you need to generate the makefiles and after that compile the software.

Generate the makefiles and compile the engine by running the commands below
```
mkdir build && cd build
cmake ..
make
```

Cross compilation from Linux to Windows can be achieved by adding the MinGW toolchain to the generator call `-DCMAKE_TOOLCHAIN_FILE=../cmake/mingw.cmake`

Once the makefiles have been generated, you may compile the stand alone binary of the engine by compiling target `samstorm`. The default target is `libsamstorm`, which only builds the library.

The git repository does not contain the resources file `resources.tar` that have been used in the `samstorm/main.cpp` file. This means that the stand alone engine build will succeed but the execution will fail. At some point some official demo graphics will be provided.
