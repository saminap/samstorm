function(add_shader_dependency target link_type)
	foreach(arg IN LISTS ARGN)
		get_filename_component(folder ${arg} DIRECTORY)
		set(shader_folder ${CMAKE_CURRENT_BINARY_DIR}/${folder})
		if(NOT EXISTS ${shader_folder})
			file(MAKE_DIRECTORY ${shader_folder})
		endif()

		string(REPLACE "/" "_" dependency ${arg})
		add_custom_target(${dependency}.dep
			DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/${arg}.o
		)

		if(${CMAKE_SYSTEM_NAME} STREQUAL "Windows")
			set(objcopy_output "elf32-i386")
			set(objcopy_extra_arg "--prefix-symbol" "_")
		else()
			set(objcopy_output "elf64-x86-64")
		endif()

		add_custom_command(
			OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/${arg}.o
			COMMAND objcopy --input binary --output ${objcopy_output} ${objcopy_extra_arg} --binary-architecture i386 ${arg} ${CMAKE_CURRENT_BINARY_DIR}/${arg}.o
			DEPENDS ${arg}
			WORKING_DIRECTORY ${CMAKE_CURRENT_LIST_DIR}
		)

		list(APPEND shader_objects ${CMAKE_CURRENT_BINARY_DIR}/${arg}.o)
		target_link_libraries(${target}
			${link_type}
				${CMAKE_CURRENT_BINARY_DIR}/${arg}.o
		)
		add_dependencies(${target} ${dependency}.dep)
	endforeach()
endfunction()
